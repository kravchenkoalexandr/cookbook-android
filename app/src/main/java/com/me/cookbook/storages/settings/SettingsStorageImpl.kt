package com.me.cookbook.storages.settings

import android.content.Context
import com.ironz.binaryprefs.BinaryPreferencesBuilder
import javax.inject.Inject

class SettingsStorageImpl @Inject constructor(
    private val context: Context
) : SettingsStorage {

    companion object {

        private const val PREFERENCES_FILENAME = "settings"
    }

    private val preferences = BinaryPreferencesBuilder(context)
        .name(PREFERENCES_FILENAME)
        .build()

    override fun getSetting(key: String): Boolean? {
        return if (preferences.contains(key)) preferences.getBoolean(key, false) else null
    }

    override fun setSetting(key: String, value: Boolean?) {
        if (value == null) {
            preferences.edit().remove(key).commit()
        } else {
            preferences.edit().putBoolean(key, value).commit()
        }
    }
}
