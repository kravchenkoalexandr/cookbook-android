package com.me.cookbook.storages.user

import io.reactivex.Observable
import java.util.*

interface UserStorage {

    data class User(val id: Int, val name: String)

    fun save(user: User)

    fun clear()

    fun observable(): Observable<User>
}
