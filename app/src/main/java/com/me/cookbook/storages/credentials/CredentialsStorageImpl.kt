package com.me.cookbook.storages.credentials

import android.content.Context
import com.ironz.binaryprefs.BinaryPreferencesBuilder
import javax.inject.Inject

class CredentialsStorageImpl @Inject constructor(
    private val context: Context
) : CredentialsStorage {

    companion object {
        private const val PREFERENCES_FILENAME = "credentials"

        private const val KEY_TOKEN = "access token"
    }

    private val preferences = BinaryPreferencesBuilder(context)
        .name(PREFERENCES_FILENAME)
        .build()

    override fun getToken(): String? {
        return preferences.getString(KEY_TOKEN, null)
    }

    override fun saveToken(token: String) {
        preferences.edit().putString(KEY_TOKEN, token).commit()
    }

    override fun deleteToken() {
        preferences.edit().putString(KEY_TOKEN, null).commit()
    }
}
