package com.me.cookbook.storages.credentials

interface CredentialsStorage {

    fun getToken(): String?

    fun saveToken(token: String)

    fun deleteToken()
}
