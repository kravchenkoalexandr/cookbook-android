package com.me.cookbook.storages.settings

object SettingsKeys {

    val isContinueWithoutLogin = "CONTINUE_WITHOUT_LOGIN_KEY"
}

interface SettingsStorage {

    fun getSetting(key: String): Boolean?

    fun setSetting(key: String, value: Boolean?)
}
