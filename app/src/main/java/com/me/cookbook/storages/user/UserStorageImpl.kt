package com.me.cookbook.storages.user

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class UserStorageImpl @Inject constructor() : UserStorage {

    private var source = BehaviorSubject.create<UserStorage.User>()

    override fun save(user: UserStorage.User) {
        source.onNext(user)
    }

    override fun clear() {
        source.onComplete()
        source = BehaviorSubject.create<UserStorage.User>()
    }

    override fun observable(): Observable<UserStorage.User> {
        return source
    }
}
