package com.me.cookbook.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.me.cookbook.R

fun makeImagePickerDialog(
    context: Context,
    layoutInflater: LayoutInflater,
    isDeleteEnabled: Boolean,
    onPhotoPickClicked: () -> Unit,
    onImagePickClicked: () -> Unit,
    onDeleteClicked: (() -> Unit)?
): AlertDialog {
    val builder = AlertDialog.Builder(context)

    val view = layoutInflater.inflate(R.layout.image_picker_dialog, null)
    var closeAction: () -> Unit = {}

    view.findViewById<TextView>(R.id.vw_pick_photo).setOnClickListener {
        onPhotoPickClicked()
        closeAction()
    }
    view.findViewById<TextView>(R.id.vw_pick_image).setOnClickListener {
        onImagePickClicked()
        closeAction()
    }
    view.findViewById<TextView>(R.id.vw_delete_image).let {
        if (isDeleteEnabled) {
            it.visibility = View.VISIBLE
            it.setOnClickListener {
                onDeleteClicked?.invoke()
                closeAction()
            }
        } else {
            it.visibility = View.GONE
        }
    }
    view.setOnClickListener {
        closeAction()
    }

    builder.setView(view)

    val dialog = builder.create()
    closeAction = { dialog.dismiss() }

    return dialog
}
