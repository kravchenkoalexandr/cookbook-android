package com.me.cookbook.utils

import com.me.cookbook.R
import com.me.cookbook.components.AppContext
import java.io.File
import java.util.*

object CacheFileProvider {

    fun createCacheImageFile(): File {
        val path = File(AppContext.instance.cacheDir, "temp/")
        if (!path.exists()) {
            path.mkdir()
        }
        val name = AppContext.instance.getString(R.string.app_name).plus(Date().toString()).plus(".jpg")
        return File(path, name)
    }
}
