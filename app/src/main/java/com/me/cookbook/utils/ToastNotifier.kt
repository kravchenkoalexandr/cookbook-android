package com.me.cookbook.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.me.cookbook.R
import kotlinx.android.synthetic.main.toast_view.view.*

enum class NotificationType {
    ERROR, SUCCESS, WARNING;

    fun getIcon(context: Context): Drawable? {
        return when (this) {
            ERROR -> ContextCompat.getDrawable(context, R.drawable.ic_notification_error)
            SUCCESS -> ContextCompat.getDrawable(context, R.drawable.ic_notification_success)
            WARNING -> ContextCompat.getDrawable(context, R.drawable.ic_notification_warning)
        }
    }

    fun getBackgroundColor(context: Context): Int {
        return when (this) {
            ERROR -> ContextCompat.getColor(context, R.color.notification_error)
            SUCCESS -> ContextCompat.getColor(context, R.color.notification_success)
            WARNING -> ContextCompat.getColor(context, R.color.notification_warning)
        }
    }
}

fun makeToastNotifier(
    root: View,
    inflater: LayoutInflater,
    context: Context,
    message: String,
    notificationType: NotificationType
): Toast {
    val layout = inflater.inflate(R.layout.toast_view, root.findViewById(R.id.ll_toast_root)).apply {

        tv_message.text = message
        ll_toast_root.background.setTint(notificationType.getBackgroundColor(context))
        tv_message.setCompoundDrawablesWithIntrinsicBounds(
            notificationType.getIcon(context),
            null,
            null,
            null
        )
    }

    return Toast(context).apply {
        duration = Toast.LENGTH_SHORT
        setGravity(Gravity.TOP, 0, 24)
        view = layout
    }
}
