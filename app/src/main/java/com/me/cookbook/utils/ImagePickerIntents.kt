package com.me.cookbook.utils

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Parcelable
import android.provider.MediaStore

object ImagePickerIntents {

    private fun getGalleriesIntents(context: Context): List<Intent> {
        val galleryIntent = Intent()
        galleryIntent.type = "image/*"
        galleryIntent.action = Intent.ACTION_GET_CONTENT

        val galleryIntents = mutableListOf<Intent>()
        val galleries = context.packageManager.queryIntentActivities(galleryIntent, 0)
        galleries.forEach {
            val packageName = it.activityInfo.packageName
            val finalIntent = Intent(galleryIntent).apply {
                component = ComponentName(
                    packageName,
                    it.activityInfo.name
                )
            }
            finalIntent.setPackage(packageName)
            galleryIntents.add(finalIntent)
        }

        return galleryIntents
    }

    fun getCameraIntent(context: Context, photoUri: Uri): Intent {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)

        return cameraIntent
    }

    fun getGalleriesIntent(context: Context, title: String): Intent {
        val galleriesIntents = getGalleriesIntents(context)

        val chooser = Intent.createChooser(Intent(Intent.ACTION_GET_CONTENT), title)
        chooser.putExtra(
            Intent.EXTRA_INITIAL_INTENTS,
            galleriesIntents.map { it as Parcelable }.toTypedArray())

        return chooser
    }

    fun getCameraAndGalleryChooser(context: Context, photoUri: Uri, title: String): Intent {
        val cameraIntent = getCameraIntent(context, photoUri)
        val galleriesIntents = getGalleriesIntents(context)

        val chooser = Intent.createChooser(cameraIntent, title)
        chooser.putExtra(
            Intent.EXTRA_INITIAL_INTENTS,
            galleriesIntents.map { it as Parcelable }.toTypedArray())

        return chooser
    }
}
