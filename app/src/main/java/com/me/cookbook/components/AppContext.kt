package com.me.cookbook.components

import android.app.Application
import com.me.cookbook.di.AppComponent
import com.me.cookbook.di.AppModule
import com.me.cookbook.di.DaggerAppComponent
import com.me.cookbook.utils.FileHelper

class AppContext : Application() {

    companion object {
        lateinit var instance: AppContext
            private set

        lateinit var appComponent: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        appComponent = initDaggerComponent()

        FileHelper.initialize(this, this.packageName + ".fileprovider")
    }

    private fun initDaggerComponent(): AppComponent {
        return DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()
    }
}
