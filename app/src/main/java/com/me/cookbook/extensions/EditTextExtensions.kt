package com.me.cookbook.extensions

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

fun EditText.focusEnd() {
    requestFocus()
    setSelection(text.length)
}

fun EditText.setOnTextChangeListener(listener: (String) -> Unit): TextWatcher {
    val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            listener.invoke(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
    }

    addTextChangedListener(textWatcher)
    return textWatcher
}
