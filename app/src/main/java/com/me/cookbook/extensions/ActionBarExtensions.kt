package com.me.cookbook.extensions

import androidx.appcompat.app.ActionBar

fun ActionBar.setDisplayHomeButtonEnabled(enable: Boolean) {
    setDisplayHomeAsUpEnabled(enable)
    setDisplayShowHomeEnabled(enable)
}
