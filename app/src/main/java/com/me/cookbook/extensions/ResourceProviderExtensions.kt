package com.me.cookbook.extensions

import android.graphics.drawable.Drawable
import com.me.cookbook.R
import com.me.cookbook.presentation.other.resources.ResourceProvider

fun ResourceProvider.getStepsCountImage(stepsCount: Int): Drawable? {
    return when (stepsCount) {
        0 -> getDrawable(R.drawable.ic_steps_0)
        1 -> getDrawable(R.drawable.ic_steps_1)
        2 -> getDrawable(R.drawable.ic_steps_2)
        3 -> getDrawable(R.drawable.ic_steps_3)
        4 -> getDrawable(R.drawable.ic_steps_4)
        5 -> getDrawable(R.drawable.ic_steps_5)
        6 -> getDrawable(R.drawable.ic_steps_6)
        7 -> getDrawable(R.drawable.ic_steps_7)
        8 -> getDrawable(R.drawable.ic_steps_8)
        else -> getDrawable(R.drawable.ic_steps_9_more)
    }
}
