package com.me.cookbook.extensions

import android.view.View

fun View.setVisibleEnable(enable: Boolean) {
    visibility = if (enable) { View.VISIBLE } else { View.GONE }
}
