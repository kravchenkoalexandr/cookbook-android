package com.me.cookbook.di.network

import com.fasterxml.jackson.databind.ObjectMapper
import com.me.cookbook.storages.credentials.CredentialsStorage
import dagger.Module
import dagger.Provides
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(credentialsStorage: CredentialsStorage): OkHttpClient {
        return OkHttpClient().newBuilder()
            .addInterceptor { chain ->
                val requestBuilder = chain.request().newBuilder()
                    .header("Content-Type", "application/json")

                credentialsStorage.getToken()?.let {
                    requestBuilder.header("Authorization", "Bearer $it")
                }

                return@addInterceptor chain.proceed(requestBuilder.build())
            }
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .readTimeout(NetworkConfig.readTimeOut, TimeUnit.SECONDS)
            .writeTimeout(NetworkConfig.writeTimeOut, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        converterFactory: Converter.Factory,
        callAdapterFactory: CallAdapter.Factory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(NetworkConfig.serverUrl)
            .client(okHttpClient)
            .addCallAdapterFactory(callAdapterFactory)
            .addConverterFactory(converterFactory)
            .build()
    }

    @Provides
    @Singleton
    fun provideConverterFactory(objectMapper: ObjectMapper): Converter.Factory {
        return JacksonConverterFactory.create(objectMapper)
    }

    @Provides
    @Singleton
    fun provideCallAdapterFactory(): CallAdapter.Factory {
        return RxJava2CallAdapterFactory.createAsync()
    }

    @Provides
    @Singleton
    fun provideObjectMapper(): ObjectMapper {
        return ObjectMapper()
    }
}
