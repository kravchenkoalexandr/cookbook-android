package com.me.cookbook.di.services

import com.me.cookbook.data.auth.ServiceAuth
import com.me.cookbook.data.auth.ServiceAuthImpl
import com.me.cookbook.data.auth.repositories.AuthRepository
import com.me.cookbook.data.auth.repositories.RetrofitAuthRepository
import com.me.cookbook.data.auth.repositories.models.AuthDataSource
import com.me.cookbook.data.chefs.ServiceChefs
import com.me.cookbook.data.chefs.ServiceChefsImpl
import com.me.cookbook.data.chefs.models.ChefsDataSource
import com.me.cookbook.data.chefs.repositories.ChefsRepository
import com.me.cookbook.data.chefs.repositories.RetrofitChefsRepository
import com.me.cookbook.data.cuisines.ServiceCuisines
import com.me.cookbook.data.cuisines.ServiceCuisinesImpl
import com.me.cookbook.data.cuisines.models.CuisinesDataSource
import com.me.cookbook.data.cuisines.repositories.CuisinesRepository
import com.me.cookbook.data.cuisines.repositories.RetrofitCuisinesRepository
import com.me.cookbook.data.files.ServiceFiles
import com.me.cookbook.data.files.ServiceFilesImpl
import com.me.cookbook.data.files.models.FilesDataSource
import com.me.cookbook.data.files.repositories.FilesRepository
import com.me.cookbook.data.files.repositories.RetrofitFilesRepository
import com.me.cookbook.data.recipes.ServiceRecipes
import com.me.cookbook.data.recipes.ServiceRecipesImpl
import com.me.cookbook.data.recipes.models.RecipesDataSource
import com.me.cookbook.data.recipes.repositories.RecipesRepository
import com.me.cookbook.data.recipes.repositories.RetrofitRecipesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import retrofit2.Retrofit

@Module
class ServicesModule {

    /** region ==================== Auth ==================== **/

    @Provides
    fun provideAuthRepository(retrofitAuthRepository: RetrofitAuthRepository): AuthRepository {
        return retrofitAuthRepository
    }

    @Provides
    fun provideAuthService(serviceAuthImpl: ServiceAuthImpl): ServiceAuth {
        return serviceAuthImpl
    }

    @Provides
    @Singleton
    fun provideAuthDataSource(retrofit: Retrofit): AuthDataSource {
        return retrofit.create(AuthDataSource::class.java)
    }

    /** endregion **/

    /** region ==================== Recipes ==================== **/

    @Provides
    fun provideRecipesRepository(retrofitRecipesRepository: RetrofitRecipesRepository): RecipesRepository {
        return retrofitRecipesRepository
    }

    @Provides
    fun provideRecipesService(serviceRecipesImpl: ServiceRecipesImpl): ServiceRecipes {
        return serviceRecipesImpl
    }

    @Provides
    @Singleton
    fun provideRecipesDataSource(retrofit: Retrofit): RecipesDataSource {
        return retrofit.create(RecipesDataSource::class.java)
    }

    /** endregion **/

    /** region ==================== Files ==================== **/

    @Provides
    fun provideFilesRepository(retrofitFilesRepository: RetrofitFilesRepository): FilesRepository {
        return retrofitFilesRepository
    }

    @Provides
    fun provideFilesService(serviceFilesImpl: ServiceFilesImpl): ServiceFiles {
        return serviceFilesImpl
    }

    @Provides
    @Singleton
    fun provideFilesDataSource(retrofit: Retrofit): FilesDataSource {
        return retrofit.create(FilesDataSource::class.java)
    }

    /** endregion **/

    /** region ==================== Chefs ==================== **/

    @Provides
    fun provideChefsRepository(retrofitChefsRepository: RetrofitChefsRepository): ChefsRepository {
        return retrofitChefsRepository
    }

    @Provides
    fun provideChefsService(serviceChefsImpl: ServiceChefsImpl): ServiceChefs {
        return serviceChefsImpl
    }

    @Provides
    @Singleton
    fun provideChefsDataSource(retrofit: Retrofit): ChefsDataSource {
        return retrofit.create(ChefsDataSource::class.java)
    }

    /** endregion **/

    /** region ==================== Cuisines ==================== **/

    @Provides
    fun provideCuisinesRepository(retrofitCuisinesRepository: RetrofitCuisinesRepository): CuisinesRepository {
        return retrofitCuisinesRepository
    }

    @Provides
    fun provideCuisinesService(serviceCuisinesImpl: ServiceCuisinesImpl): ServiceCuisines {
        return serviceCuisinesImpl
    }

    @Provides
    @Singleton
    fun provideCuisinesDataSource(retrofit: Retrofit): CuisinesDataSource {
        return retrofit.create(CuisinesDataSource::class.java)
    }

    /** endregion **/
}
