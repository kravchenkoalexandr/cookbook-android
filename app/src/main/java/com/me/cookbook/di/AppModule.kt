package com.me.cookbook.di

import android.content.Context
import com.me.cookbook.components.AppContext
import com.me.cookbook.presentation.other.resources.ResourceProvider
import com.me.cookbook.presentation.other.resources.ResourceProviderImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: AppContext) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideResourceProvider(resourceProviderImpl: ResourceProviderImpl): ResourceProvider {
        return resourceProviderImpl
    }
}
