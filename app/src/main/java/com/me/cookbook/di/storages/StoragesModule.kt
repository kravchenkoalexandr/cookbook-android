package com.me.cookbook.di.storages

import com.me.cookbook.storages.credentials.CredentialsStorage
import com.me.cookbook.storages.credentials.CredentialsStorageImpl
import com.me.cookbook.storages.settings.SettingsStorage
import com.me.cookbook.storages.settings.SettingsStorageImpl
import com.me.cookbook.storages.user.UserStorage
import com.me.cookbook.storages.user.UserStorageImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StoragesModule {

    @Provides
    @Singleton
    fun provideCredentialsStorage(credentialsStorageImpl: CredentialsStorageImpl): CredentialsStorage {
        return credentialsStorageImpl
    }

    @Provides
    @Singleton
    fun provideSettingsStorage(settingsStorageImpl: SettingsStorageImpl): SettingsStorage {
        return settingsStorageImpl
    }

    @Provides
    @Singleton
    fun provideUserStorage(userStorageImpl: UserStorageImpl): UserStorage {
        return userStorageImpl
    }
}
