package com.me.cookbook.di

import com.me.cookbook.di.network.NetworkModule
import com.me.cookbook.di.services.ServicesModule
import com.me.cookbook.di.storages.StoragesModule
import com.me.cookbook.presentation.activities.home.HomeFlowActivityComponent
import com.me.cookbook.presentation.activities.splash.SplashActivityComponent
import com.me.cookbook.presentation.fragments.auth.login.LoginFragmentComponent
import com.me.cookbook.presentation.fragments.auth.registration.RegistrationFragmentComponent
import com.me.cookbook.presentation.fragments.home.HomeFragmentComponent
import com.me.cookbook.presentation.fragments.home.HomeFragmentModule
import com.me.cookbook.presentation.fragments.main.MainFragmentComponent
import com.me.cookbook.presentation.fragments.main.MainFragmentModule
import com.me.cookbook.presentation.fragments.preload.PreloadFragmentComponent
import com.me.cookbook.presentation.fragments.recipe.edit.RecipeEditorFragmentComponent
import com.me.cookbook.presentation.fragments.recipe.edit.RecipeEditorFragmentModule
import com.me.cookbook.presentation.fragments.recipe.view.RecipeFragmentComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class, StoragesModule::class, NetworkModule::class, ServicesModule::class
])
interface AppComponent {

    fun splashActivityComponent(): SplashActivityComponent

    fun homeFlowActivityComponent(): HomeFlowActivityComponent

    fun preloadFragmentComponent(): PreloadFragmentComponent

    fun loginFragmentComponent(): LoginFragmentComponent

    fun registrationFragmentComponent(): RegistrationFragmentComponent

    fun mainFragmentComponent(module: MainFragmentModule): MainFragmentComponent

    fun recipeFragmentComponent(): RecipeFragmentComponent

    fun homeFragmentComponent(module: HomeFragmentModule): HomeFragmentComponent

    fun recipeEditorComponent(module: RecipeEditorFragmentModule): RecipeEditorFragmentComponent
}
