package com.me.cookbook.di.network

object NetworkConfig {

    const val serverUrl = "http://10.0.2.2:8080"

    const val readTimeOut = 30L

    const val writeTimeOut = 30L
}
