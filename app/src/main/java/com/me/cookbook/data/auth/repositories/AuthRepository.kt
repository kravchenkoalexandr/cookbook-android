package com.me.cookbook.data.auth.repositories

import com.me.cookbook.data.auth.repositories.models.AuthResponse
import io.reactivex.Single

interface AuthRepository {

    fun login(email: String, password: String): Single<AuthResponse>

    fun register(name: String, email: String, password: String): Single<AuthResponse>
}
