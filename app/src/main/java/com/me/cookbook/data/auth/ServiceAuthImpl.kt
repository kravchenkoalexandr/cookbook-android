package com.me.cookbook.data.auth

import com.me.cookbook.data.auth.repositories.AuthRepository
import com.me.cookbook.storages.credentials.CredentialsStorage
import com.me.cookbook.storages.settings.SettingsKeys
import com.me.cookbook.storages.settings.SettingsStorage
import com.me.cookbook.storages.user.UserStorage
import io.reactivex.Completable
import javax.inject.Inject

class ServiceAuthImpl @Inject constructor(
    private val authRepository: AuthRepository,
    private val credentialsStorage: CredentialsStorage,
    private val userStorage: UserStorage,
    private val settingsStorage: SettingsStorage
) : ServiceAuth {

    override fun login(email: String, password: String): Completable {
        return authRepository
            .login(email, password)
            .doOnSuccess { (user, access) ->
                loginCommand(user.id, user.name, access.token)
            }
            .flatMapCompletable { Completable.complete() }
    }

    override fun register(name: String, email: String, password: String): Completable {
        return authRepository
            .register(name, email, password)
            .doOnSuccess { (user, access) ->
                loginCommand(user.id, user.name, access.token)
            }
            .flatMapCompletable { Completable.complete() }
    }

    override fun isAuthorized(): Boolean {
        return credentialsStorage.getToken() != null
    }

    override fun logout() {
        credentialsStorage.deleteToken()
        userStorage.clear()
        settingsStorage.setSetting(SettingsKeys.isContinueWithoutLogin, null)
    }

    private fun loginCommand(id: Int, name: String, token: String) {
        credentialsStorage.saveToken(token)
        userStorage.save(UserStorage.User(id, name))
        settingsStorage.setSetting(SettingsKeys.isContinueWithoutLogin, null)
    }
}
