package com.me.cookbook.data.auth.repositories.models

import com.fasterxml.jackson.annotation.JsonProperty

data class RegistrationRequest(
    @JsonProperty("name") val name: String,
    @JsonProperty("email") val email: String,
    @JsonProperty("password") val password: String
)
