package com.me.cookbook.data.chefs.repositories

import com.me.cookbook.data.chefs.models.ChefResponse
import com.me.cookbook.data.chefs.models.ChefsDataSource
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RetrofitChefsRepository @Inject constructor(
    private val chefsDataSource: ChefsDataSource
) : ChefsRepository {

    override fun getAll(): Single<List<ChefResponse>> {
        return chefsDataSource.getAll()
            .subscribeOn(Schedulers.io())
    }
}
