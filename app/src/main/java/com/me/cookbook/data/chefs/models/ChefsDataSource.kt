package com.me.cookbook.data.chefs.models

import io.reactivex.Single
import retrofit2.http.GET

interface ChefsDataSource {

    @GET("/chefs")
    fun getAll(): Single<List<ChefResponse>>
}
