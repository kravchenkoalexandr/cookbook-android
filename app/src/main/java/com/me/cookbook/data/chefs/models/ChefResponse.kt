package com.me.cookbook.data.chefs.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class ChefResponse(
    @JsonProperty("id") val id: Int,
    @JsonProperty("fullName") val fullName: String
)
