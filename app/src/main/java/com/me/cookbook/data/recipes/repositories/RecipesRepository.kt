package com.me.cookbook.data.recipes.repositories

import com.me.cookbook.data.base.PagingResult
import com.me.cookbook.data.recipes.models.RecipeAddRequest
import com.me.cookbook.data.recipes.models.RecipeResponse
import com.me.cookbook.data.recipes.models.RecipeUpdateRequest
import io.reactivex.Single

interface RecipesRepository {

    fun search(query: String, page: Int, pageSize: Int): Single<PagingResult<RecipeResponse>>

    fun getUserRecipes(
        userId: Int,
        page: Int,
        pageSize: Int
    ): Single<PagingResult<RecipeResponse>>

    fun getRecipeById(id: Int): Single<RecipeResponse>

    fun add(recipeAddRequest: RecipeAddRequest): Single<RecipeResponse>

    fun update(recipeUpdateRequest: RecipeUpdateRequest): Single<RecipeResponse>
}
