package com.me.cookbook.data.auth.repositories

import com.me.cookbook.data.auth.repositories.models.AuthDataSource
import com.me.cookbook.data.auth.repositories.models.AuthResponse
import com.me.cookbook.data.auth.repositories.models.LoginRequest
import com.me.cookbook.data.auth.repositories.models.RegistrationRequest
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RetrofitAuthRepository @Inject constructor(
    private val authDataSource: AuthDataSource
) : AuthRepository {

    override fun login(email: String, password: String): Single<AuthResponse> {
        return authDataSource.login(LoginRequest(email, password))
            .subscribeOn(Schedulers.io())
    }

    override fun register(name: String, email: String, password: String): Single<AuthResponse> {
        return authDataSource.register(RegistrationRequest(name, email, password))
            .subscribeOn(Schedulers.io())
    }
}
