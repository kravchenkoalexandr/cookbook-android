package com.me.cookbook.data.base

import io.reactivex.Observable

interface Paginator<T> {

    fun canLoadNext(): Boolean

    fun next()

    fun observable(): Observable<List<T>>

    fun reset()
}
