package com.me.cookbook.data.chefs

import com.me.cookbook.data.chefs.models.ChefResponse
import io.reactivex.Single

interface ServiceChefs {

    fun getAll(): Single<List<ChefResponse>>
}
