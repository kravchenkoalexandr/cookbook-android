package com.me.cookbook.data.recipes.models

import com.me.cookbook.data.base.PagingResult
import io.reactivex.Single
import retrofit2.http.*

interface RecipesDataSource {

    @GET("/recipes")
    fun search(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int
    ): Single<PagingResult<RecipeResponse>>

    @GET("/users/{id}/recipes")
    fun getUserRecipes(
        @Path("id") userId: Int,
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int
    ): Single<PagingResult<RecipeResponse>>

    @GET("/recipes/{id}")
    fun getRecipeById(@Path("id") id: Int): Single<RecipeResponse>

    @POST("/recipes")
    fun add(@Body recipeAddRequest: RecipeAddRequest): Single<RecipeResponse>

    @PUT("/recipes")
    fun update(@Body recipeUpdateRequest: RecipeUpdateRequest): Single<RecipeResponse>
}
