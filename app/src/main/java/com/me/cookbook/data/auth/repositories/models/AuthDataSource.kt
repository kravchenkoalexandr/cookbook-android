package com.me.cookbook.data.auth.repositories.models

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthDataSource {

    @POST("/login")
    fun login(@Body loginRequest: LoginRequest): Single<AuthResponse>

    @POST("/register")
    fun register(@Body registrationRequest: RegistrationRequest): Single<AuthResponse>
}
