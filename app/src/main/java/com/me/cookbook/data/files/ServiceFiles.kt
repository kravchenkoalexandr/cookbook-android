package com.me.cookbook.data.files

import io.reactivex.Single
import java.io.File

interface ServiceFiles {

    fun upload(file: File): Single<String>
}
