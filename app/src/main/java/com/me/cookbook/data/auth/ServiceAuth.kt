package com.me.cookbook.data.auth

import io.reactivex.Completable

interface ServiceAuth {

    fun login(email: String, password: String): Completable

    fun register(name: String, email: String, password: String): Completable

    fun logout()

    fun isAuthorized(): Boolean
}
