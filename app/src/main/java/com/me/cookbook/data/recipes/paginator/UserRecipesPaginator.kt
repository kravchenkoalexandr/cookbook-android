package com.me.cookbook.data.recipes.paginator

import com.me.cookbook.data.base.Paginator
import com.me.cookbook.data.recipes.ServiceRecipes
import com.me.cookbook.data.recipes.models.RecipeResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class UserRecipesPaginator @Inject constructor(
    private val serviceRecipes: ServiceRecipes
) : Paginator<RecipeResponse> {

    companion object {

        private val DEFAULT_PAGE_SIZE = 25
    }

    /** region ==================== Variables ==================== **/

    var userId: Int? = null
        private set

    var page = 1
        private set

    var pageSize = DEFAULT_PAGE_SIZE
        private set

    val totalCount
        get() = pagingState.count

    private var source = BehaviorSubject.create<List<RecipeResponse>>()
    private var loadDisposable: Disposable? = null

    private var hasMore: Boolean = true
    private var pagingState = PagingState()

    /** endregion **/

    /** region ==================== Inner class ==================== **/

    data class PagingState(val items: List<RecipeResponse> = emptyList(), val count: Int = 0)

    /** endregion **/

    /** region ==================== Paginator ==================== **/

    override fun canLoadNext(): Boolean {
        return hasMore && loadDisposable == null && userId != null
    }

    override fun observable(): Observable<List<RecipeResponse>> {
        return source
    }

    override fun next() {
        val userId: Int? = this.userId
        if (!canLoadNext() || userId == null) {
            return
        }

        loadDisposable = serviceRecipes.getUserRecipes(userId, page, pageSize)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                hasMore = result.totalItems >= pageSize

                val newItems = result.items.filter { item -> pagingState.items.none { it.id == item.id } }

                pagingState = PagingState(pagingState.items.plus(newItems), totalCount + newItems.size)
                loadDisposable = null
                page++

                source.onNext(pagingState.items)
            }, { throwable ->
                loadDisposable = null
                hasMore = false

                source.onError(throwable)
            })
    }

    override fun reset() {
        loadDisposable?.dispose()
        loadDisposable = null

        source = BehaviorSubject.create<List<RecipeResponse>>()

        pagingState = PagingState()
        hasMore = true
        page = 1
        pageSize = DEFAULT_PAGE_SIZE
    }

    fun setNewQuery(userId: Int, page: Int = 1, pageSize: Int = DEFAULT_PAGE_SIZE): Boolean {
        return if (userId != this.userId) {
            reset()
            this.userId = userId
            this.page = page
            this.pageSize = pageSize

            true
        } else { false }
    }

    /** endregion **/
}
