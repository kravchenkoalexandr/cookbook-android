package com.me.cookbook.data.files.repositories

import io.reactivex.Single
import java.io.File

interface FilesRepository {

    fun upload(file: File): Single<String>
}
