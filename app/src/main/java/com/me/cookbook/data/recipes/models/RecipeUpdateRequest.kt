package com.me.cookbook.data.recipes.models

import com.fasterxml.jackson.annotation.JsonProperty

data class RecipeUpdateRequest(
    @JsonProperty("id") val id: Int,
    @JsonProperty("title") val title: String,
    @JsonProperty("description") val description: String?,
    @JsonProperty("steps") val steps: List<Step>,
    @JsonProperty("imageUrl") val imageUrl: String?,
    @JsonProperty("chef") val chef: Chef?,
    @JsonProperty("nationalCuisine") val nationalCuisine: NationalCuisine?
) {
    data class Step(
        @JsonProperty("id") val id: Int?,
        @JsonProperty("title") val title: String,
        @JsonProperty("body") val body: String,
        @JsonProperty("number") val number: Int,
        @JsonProperty("imageUrl") val imageUrl: String?
    )

    data class Chef(
        @JsonProperty("id") val id: Int?,
        @JsonProperty("fullName") val fullName: String
    )

    data class NationalCuisine(
        @JsonProperty("id") val id: Int?,
        @JsonProperty("name") val name: String
    )
}
