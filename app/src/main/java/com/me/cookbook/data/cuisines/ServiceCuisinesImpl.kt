package com.me.cookbook.data.cuisines

import com.me.cookbook.data.cuisines.models.CuisinesResponse
import com.me.cookbook.data.cuisines.repositories.CuisinesRepository
import io.reactivex.Single
import javax.inject.Inject

class ServiceCuisinesImpl @Inject constructor(
    private val cuisinesRepository: CuisinesRepository
) : ServiceCuisines {

    override fun getAll(): Single<List<CuisinesResponse>> {
        return cuisinesRepository.getAll()
    }
}
