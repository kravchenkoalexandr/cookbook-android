package com.me.cookbook.data.chefs

import com.me.cookbook.data.chefs.models.ChefResponse
import com.me.cookbook.data.chefs.repositories.ChefsRepository
import io.reactivex.Single
import javax.inject.Inject

class ServiceChefsImpl @Inject constructor(
    private val chefsRepository: ChefsRepository
) : ServiceChefs {

    override fun getAll(): Single<List<ChefResponse>> {
        return chefsRepository.getAll()
    }
}
