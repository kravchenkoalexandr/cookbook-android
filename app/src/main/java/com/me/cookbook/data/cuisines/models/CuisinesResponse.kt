package com.me.cookbook.data.cuisines.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class CuisinesResponse(
    @JsonProperty("id") val id: Int,
    @JsonProperty("name") val name: String
)
