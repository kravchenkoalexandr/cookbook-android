package com.me.cookbook.data.cuisines.repositories

import com.me.cookbook.data.cuisines.models.CuisinesDataSource
import com.me.cookbook.data.cuisines.models.CuisinesResponse
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RetrofitCuisinesRepository @Inject constructor(
    private val cuisinesDataSource: CuisinesDataSource
) : CuisinesRepository {

    override fun getAll(): Single<List<CuisinesResponse>> {
        return cuisinesDataSource.getAll()
            .subscribeOn(Schedulers.io())
    }
}
