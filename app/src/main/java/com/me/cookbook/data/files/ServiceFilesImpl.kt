package com.me.cookbook.data.files

import com.me.cookbook.data.files.repositories.FilesRepository
import io.reactivex.Single
import java.io.File
import javax.inject.Inject

class ServiceFilesImpl @Inject constructor(
    private val filesRepository: FilesRepository
) : ServiceFiles {

    override fun upload(file: File): Single<String> {
        return filesRepository.upload(file)
    }
}
