package com.me.cookbook.data.recipes.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonIgnoreProperties(ignoreUnknown = true)
data class RecipeResponse(
    @JsonProperty("id") val id: Int,
    @JsonProperty("title") val title: String,
    @JsonProperty("description") val description: String?,
    @JsonProperty("author") val author: Author,
    @JsonProperty("stepsCount") val stepsCount: Int,
    @JsonProperty("likesCount") val likesCount: Int,
    @JsonProperty("steps") val steps: List<Step>,
    @JsonProperty("imageUrl") val imageUrl: String?,
    @JsonProperty("liked") val liked: Boolean? = null,
    @JsonProperty("chef") val chef: Chef?,
    @JsonProperty("nationalCuisine") val nationalCuisine: NationalCuisine?
) : Serializable {

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Author(
        @JsonProperty("id") val id: Int,
        @JsonProperty("name") val name: String
    ) : Serializable

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Chef(
        @JsonProperty("id") val id: Int,
        @JsonProperty("fullName") val fullName: String
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class NationalCuisine(
        @JsonProperty("id") val id: Int,
        @JsonProperty("name") val name: String
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Step(
        @JsonProperty("id") val id: Int,
        @JsonProperty("title") val title: String,
        @JsonProperty("body") val body: String,
        @JsonProperty("recipeId") val recipeId: Int,
        @JsonProperty("number") val number: Int,
        @JsonProperty("imageUrl") val imageUrl: String?
    ) : Serializable
}
