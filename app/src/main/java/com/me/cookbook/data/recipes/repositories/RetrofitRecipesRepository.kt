package com.me.cookbook.data.recipes.repositories

import com.me.cookbook.data.base.PagingResult
import com.me.cookbook.data.recipes.models.RecipeAddRequest
import com.me.cookbook.data.recipes.models.RecipeResponse
import com.me.cookbook.data.recipes.models.RecipeUpdateRequest
import com.me.cookbook.data.recipes.models.RecipesDataSource
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RetrofitRecipesRepository @Inject constructor(
    private val recipesDataSource: RecipesDataSource
) : RecipesRepository {

    override fun search(query: String, page: Int, pageSize: Int): Single<PagingResult<RecipeResponse>> {
        return recipesDataSource.search(query, page, pageSize)
            .subscribeOn(Schedulers.io())
    }

    override fun getUserRecipes(
        userId: Int,
        page: Int,
        pageSize: Int
    ): Single<PagingResult<RecipeResponse>> {
        return recipesDataSource.getUserRecipes(userId, page, pageSize)
            .subscribeOn(Schedulers.io())
    }

    override fun getRecipeById(id: Int): Single<RecipeResponse> {
        return recipesDataSource.getRecipeById(id)
            .subscribeOn(Schedulers.io())
    }

    override fun add(recipeAddRequest: RecipeAddRequest): Single<RecipeResponse> {
        return recipesDataSource.add(recipeAddRequest)
            .subscribeOn(Schedulers.io())
    }

    override fun update(recipeUpdateRequest: RecipeUpdateRequest): Single<RecipeResponse> {
        return recipesDataSource.update(recipeUpdateRequest)
            .subscribeOn(Schedulers.io())
    }
}
