package com.me.cookbook.data.files.repositories

import android.content.Context
import com.me.cookbook.data.files.models.FilesDataSource
import com.me.cookbook.utils.FileHelper
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.io.FileNotFoundException
import java.lang.Exception
import java.util.*
import javax.inject.Inject
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody

class RetrofitFilesRepository @Inject constructor(
    private val filesDataSource: FilesDataSource,
    private val context: Context
) : FilesRepository {

    override fun upload(file: File): Single<String> {
        try {
            val fileUri = FileHelper.getFileUri(file)
            val fileType = context.contentResolver.getType(fileUri)
                ?: return Single.error(FileNotFoundException())

            val requestFile = RequestBody.create(MediaType.parse(fileType), file)

            val body = MultipartBody.Part.createFormData("file", file.name, requestFile)

            return filesDataSource.upload(body)
                .map { it.url }
                .subscribeOn(Schedulers.io())
        } catch (e: Exception) {
            return Single.error(e)
        }
    }
}
