package com.me.cookbook.data.files.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class FileUploadResponse(@JsonProperty("url") val url: String)
