package com.me.cookbook.data.files.models

import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface FilesDataSource {

    @Multipart
    @POST("/files/upload")
    fun upload(@Part file: MultipartBody.Part): Single<FileUploadResponse>
}
