package com.me.cookbook.data.chefs.repositories

import com.me.cookbook.data.chefs.models.ChefResponse
import io.reactivex.Single

interface ChefsRepository {

    fun getAll(): Single<List<ChefResponse>>
}
