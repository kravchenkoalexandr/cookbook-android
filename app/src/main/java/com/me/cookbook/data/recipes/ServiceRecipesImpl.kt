package com.me.cookbook.data.recipes

import com.me.cookbook.data.base.PagingResult
import com.me.cookbook.data.recipes.models.RecipeAddRequest
import com.me.cookbook.data.recipes.models.RecipeResponse
import com.me.cookbook.data.recipes.models.RecipeUpdateRequest
import com.me.cookbook.data.recipes.repositories.RecipesRepository
import io.reactivex.Single
import javax.inject.Inject

class ServiceRecipesImpl @Inject constructor(
    private val recipesRepository: RecipesRepository
) : ServiceRecipes {

    override fun search(
        query: String,
        page: Int,
        pageSize: Int
    ): Single<PagingResult<RecipeResponse>> {
        return recipesRepository.search(query, page, pageSize)
    }

    override fun getUserRecipes(
        userId: Int,
        page: Int,
        pageSize: Int
    ): Single<PagingResult<RecipeResponse>> {
        return recipesRepository.getUserRecipes(userId, page, pageSize)
    }

    override fun getRecipeById(id: Int): Single<RecipeResponse> {
        return recipesRepository.getRecipeById(id)
    }

    override fun add(recipeAddRequest: RecipeAddRequest): Single<RecipeResponse> {
        return recipesRepository.add(recipeAddRequest)
    }

    override fun update(recipeUpdateRequest: RecipeUpdateRequest): Single<RecipeResponse> {
        return recipesRepository.update(recipeUpdateRequest)
    }
}
