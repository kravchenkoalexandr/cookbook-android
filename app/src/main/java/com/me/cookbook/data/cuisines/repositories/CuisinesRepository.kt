package com.me.cookbook.data.cuisines.repositories

import com.me.cookbook.data.cuisines.models.CuisinesResponse
import io.reactivex.Single

interface CuisinesRepository {

    fun getAll(): Single<List<CuisinesResponse>>
}
