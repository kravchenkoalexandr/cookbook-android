package com.me.cookbook.data.auth.repositories.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class AuthResponse(
    @JsonProperty("user") val user: User,
    @JsonProperty("access") val access: Access
) {

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class User(
        @JsonProperty("id") val id: Int,
        @JsonProperty("name") val name: String
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Access(
        @JsonProperty("token") val token: String
    )
}
