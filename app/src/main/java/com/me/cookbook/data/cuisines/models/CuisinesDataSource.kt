package com.me.cookbook.data.cuisines.models

import io.reactivex.Single
import retrofit2.http.GET

interface CuisinesDataSource {

    @GET("/cuisines")
    fun getAll(): Single<List<CuisinesResponse>>
}
