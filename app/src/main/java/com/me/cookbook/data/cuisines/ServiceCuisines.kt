package com.me.cookbook.data.cuisines

import com.me.cookbook.data.cuisines.models.CuisinesResponse
import io.reactivex.Single

interface ServiceCuisines {

    fun getAll(): Single<List<CuisinesResponse>>
}
