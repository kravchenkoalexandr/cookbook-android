package com.me.cookbook.presentation.fragments.recipe.edit

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.me.cookbook.R
import com.me.cookbook.components.AppContext
import com.me.cookbook.data.chefs.models.ChefResponse
import com.me.cookbook.data.cuisines.models.CuisinesResponse
import com.me.cookbook.data.recipes.models.RecipeResponse
import com.me.cookbook.presentation.fragments.recipe.edit.adapter.RecipeItemListener
import com.me.cookbook.presentation.fragments.recipe.edit.adapter.RecipeListAdapter
import com.me.cookbook.presentation.fragments.recipe.edit.adapter.RecipeViewModel
import com.me.cookbook.utils.*
import java.util.*
import javax.inject.Inject
import pub.devrel.easypermissions.EasyPermissions

class RecipeEditorFragment : Fragment(), RecipeEditorContract.View, EasyPermissions.PermissionCallbacks {

    companion object {

        private const val IMAGE_PICK_CODE = 1133

        private const val PHOTO_PICK_CODE = 3311

        private const val EXTERNAL_STORAGE_PERMISSION_CODE = 3131
    }

    @Inject
    lateinit var presenter: RecipeEditorContract.Presenter

    @Inject
    lateinit var adapter: RecipeListAdapter

    private lateinit var vwRoot: View
    private lateinit var vwRecycler: RecyclerView
    private lateinit var vwEmpty: TextView
    private lateinit var btnAdd: FloatingActionButton
    private lateinit var btnAccept: FloatingActionButton

    private var recipe: RecipeResponse? = null
    private var cacheFileUri: Uri? = null

    private var onReadExternalStorageGranted: (() -> Unit)? = null

    /** region ==================== Lifecycle ==================== **/

    override fun onCreate(savedInstanceState: Bundle?) {
        configureDI()
        recipe = arguments?.getSerializable("recipe") as RecipeResponse?
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.recipe_editor_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureUI(view)

        presenter.attachView(this)

        if (recipe != null) {
            presenter.onViewDidLoadWithEditMode(recipe!!)
        } else {
            presenter.onViewDidLoadWithAddMode()
        }
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                IMAGE_PICK_CODE -> {
                    data?.data?.let { presenter.onImagePicked(it) }
                }
                PHOTO_PICK_CODE -> {
                    cacheFileUri?.let { presenter.onImagePicked(it) }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        when (requestCode) {
            EXTERNAL_STORAGE_PERMISSION_CODE -> {
                onReadExternalStorageGranted?.invoke()
                onReadExternalStorageGranted = null
            }
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        when (requestCode) {
            EXTERNAL_STORAGE_PERMISSION_CODE -> {
                showError(getString(R.string.recipe_editor_screen_read_external_storage_permission_not_granted))
            }
        }
    }

    /** endregion **/

    /** region ==================== Configure ==================== **/

    private fun configureDI() {
        AppContext.appComponent
            .recipeEditorComponent(RecipeEditorFragmentModule(recipeItemClickListener))
            .inject(this)
    }

    private fun configureUI(view: View) {
        vwRoot = view.findViewById(R.id.vw_root)
        vwRecycler = view.findViewById(R.id.rv_recipe)
        vwEmpty = view.findViewById(R.id.vw_empty)
        btnAccept = view.findViewById(R.id.btn_accept)
        btnAdd = view.findViewById(R.id.btn_add)

        btnAccept.setOnClickListener {
            presenter.onAcceptButtonClicked()
        }

        btnAdd.setOnClickListener {
            presenter.onStepAddButtonClicked()
        }

        vwRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        vwRecycler.adapter = adapter
        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(vwRecycler)
    }

    /** endregion **/

    /** region ==================== Contract ==================== **/

    override fun showSuccess(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.SUCCESS
        ).show()
    }

    override fun showError(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.ERROR
        ).show()
    }

    override fun updateRecipe(viewModels: List<RecipeViewModel>) {
        adapter.updateItems(viewModels)
    }

    override fun pickImage(isDeleteEnabled: Boolean) {
        if (EasyPermissions.hasPermissions(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            showImagePicker(isDeleteEnabled)
        } else {
            onReadExternalStorageGranted = { showImagePicker(isDeleteEnabled) }
            requestExternalStoragePermission()
        }
    }

    override fun getRecipe(): List<RecipeViewModel> {
        return adapter.getViewModels()
    }

    override fun setEmptyViewVisibility(isVisible: Boolean) {
        if (isVisible) {
            vwRecycler.visibility = View.GONE
            vwEmpty.visibility = View.VISIBLE
        } else {
            vwRecycler.visibility = View.VISIBLE
            vwEmpty.visibility = View.GONE
        }
    }

    override fun setChefs(chefs: List<ChefResponse>) {
        adapter.updateChefs(chefs)
    }

    override fun setCuisines(cuisines: List<CuisinesResponse>) {
        adapter.updateCuisines(cuisines)
    }

    /** endregion **/

    /** region ==================== Listeners ==================== **/

    private val recipeItemClickListener = object : RecipeItemListener {

        override fun onImageClick(viewModel: RecipeViewModel) {
            presenter.onRecipeImageClicked(viewModel)
        }

        override fun onItemRemoved(viewModel: RecipeViewModel) {
            presenter.onItemRemoved(viewModel)
        }

        override fun onItemMoved(viewModel: RecipeViewModel) {
            presenter.onItemMoved(viewModel)
        }
    }

    private val itemTouchHelperCallback = object : ItemTouchHelper.Callback() {

        override fun getMovementFlags(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder
        ): Int {
            val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
            val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
            return if (viewHolder.itemViewType == RecipeListAdapter.Type.RECIPE_HEADER) { 0 } else {
                makeMovementFlags(dragFlags, swipeFlags)
            }
        }

        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            return if (target is RecipeListAdapter.ViewHolderRecipeHeader) false
                else adapter.moveItem(viewHolder.adapterPosition, target.adapterPosition)
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            adapter.removeItem(viewHolder.adapterPosition)
        }

        override fun isItemViewSwipeEnabled(): Boolean {
            return true
        }

        override fun isLongPressDragEnabled(): Boolean {
            return true
        }
    }

    /** endregion **/

    /** region ==================== Private ==================== **/

    private fun requestExternalStoragePermission() {
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.recipe_editor_read_external_storage_permission_request_rational),
            EXTERNAL_STORAGE_PERMISSION_CODE,
            Manifest.permission.READ_EXTERNAL_STORAGE)
    }

    private fun showImagePicker(isDeleteEnabled: Boolean) {
        makeImagePickerDialog(
            requireContext(),
            layoutInflater,
            isDeleteEnabled,
            {
                val cacheFile = CacheFileProvider.createCacheImageFile()
                cacheFileUri = FileHelper.getFileUri(cacheFile)
                val intent = ImagePickerIntents.getCameraIntent(requireContext(), cacheFileUri!!)

                startActivityForResult(intent, PHOTO_PICK_CODE)
            },
            {
                val intent = ImagePickerIntents.getGalleriesIntent(
                    requireContext(),
                    getString(R.string.image_picker_galleries_apps_choose))
                startActivityForResult(intent, IMAGE_PICK_CODE)
            },
            {
                presenter.onImageDeleted()
            }
        ).show()
    }

    /** endregion **/
}
