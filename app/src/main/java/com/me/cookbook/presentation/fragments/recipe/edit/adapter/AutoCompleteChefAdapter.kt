package com.me.cookbook.presentation.fragments.recipe.edit.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.me.cookbook.R
import com.me.cookbook.data.chefs.models.ChefResponse
import java.util.*

class AutoCompleteChefAdapter(context: Context, chefs: List<ChefResponse>) :
    ArrayAdapter<ChefResponse>(context, 0, chefs) {

    private var query: String = ""
    var items = chefs.toList()
        private set
    private var itemsToDisplay = chefs.toList()

    fun setItems(items: List<ChefResponse>) {
        this.items = items.toList()
        itemsToDisplay = items.toList()

        notifyDataSetChanged()

        filter.filter(query)
    }

    fun showAll() {
        query = ""
        itemsToDisplay = items.toList()

        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        return filter
    }

    override fun getCount(): Int {
        return itemsToDisplay.size
    }

    override fun getItem(position: Int): ChefResponse? {
        return itemsToDisplay.getOrNull(position)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(context).inflate(
            R.layout.autocomplete_chef_list_item, parent, false)
        val tvAuthor: TextView = view.findViewById<TextView>(R.id.tv_chef)

        val item = itemsToDisplay[position]
        tvAuthor.text = item.fullName

        return view
    }

    private val filter = object : Filter() {

        private val filterResults = FilterResults()

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filterResult = mutableListOf<ChefResponse>()

            if (constraint.isNullOrBlank()) {
                filterResult.addAll(items)
            } else {
                query = constraint.toString().toLowerCase(Locale.getDefault()).trim()

                filterResult.addAll(items.filter {
                    it.fullName.toLowerCase(Locale.getDefault()).contains(query)
                })
            }

            itemsToDisplay = filterResult.toList()

            return filterResults.also { it.values = itemsToDisplay }
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            notifyDataSetChanged()
        }

        override fun convertResultToString(resultValue: Any?): CharSequence {
            return (resultValue as? ChefResponse)?.fullName ?: ""
        }
    }
}
