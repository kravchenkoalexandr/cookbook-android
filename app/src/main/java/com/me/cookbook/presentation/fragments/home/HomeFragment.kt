package com.me.cookbook.presentation.fragments.home

import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.me.cookbook.R
import com.me.cookbook.components.AppContext
import com.me.cookbook.presentation.fragments.home.adapter.RecipeItemClickListener
import com.me.cookbook.presentation.fragments.home.adapter.RecipeListAdapterListener
import com.me.cookbook.presentation.fragments.home.adapter.RecipeViewModel
import com.me.cookbook.presentation.fragments.home.adapter.RecipesListAdapter
import com.me.cookbook.utils.NotificationType
import com.me.cookbook.utils.makeToastNotifier
import javax.inject.Inject

class HomeFragment : Fragment(), HomeContract.View {

    @Inject
    lateinit var presenter: HomeContract.Presenter

    @Inject
    lateinit var adapter: RecipesListAdapter

    private lateinit var vwRoot: View
    private lateinit var vwRecycler: RecyclerView
    private lateinit var vwEmpty: TextView
    private lateinit var btnAdd: FloatingActionButton
    private lateinit var vwSearch: SearchView

    /** region ==================== Lifecycle ==================== **/

    override fun onCreate(savedInstanceState: Bundle?) {
        configureDI()
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureUI(view)

        presenter.attachView(this)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_recipes_toolbar_menu, menu)

        vwSearch = menu.findItem(R.id.action_search).actionView as SearchView
        vwSearch.setOnQueryTextListener(onQueryTextListener)
        vwSearch.queryHint = requireActivity().getString(R.string.search_view_hint)

        menu.findItem(R.id.action_login).isVisible = false

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_update -> {
                presenter.onReloadClicked()
                true
            }
            R.id.action_logout -> {
                presenter.onLogoutClicked()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /** endregion **/

    /** region ==================== Configure ==================== **/

    private fun configureDI() {
        AppContext
            .appComponent
            .homeFragmentComponent(
                HomeFragmentModule(recipeItemClickListener, recipeListAdapterListener)
            )
            .inject(this)
    }

    private fun configureUI(view: View) {
        vwRoot = view.findViewById(R.id.vw_root)
        vwRecycler = view.findViewById(R.id.rv_user_recipes)
        vwEmpty = view.findViewById(R.id.vw_empty)

        btnAdd = view.findViewById(R.id.btn_add)

        btnAdd.setOnClickListener {
            presenter.onRecipeAddClicked()
        }

        vwRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        vwRecycler.adapter = adapter
        vwRecycler.addOnScrollListener(onScrollListener)
    }

    /** endregion **/

    /** region ==================== Contract ==================== **/

    override fun showSuccess(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.SUCCESS
        ).show()
    }

    override fun showError(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.ERROR
        ).show()
    }

    override fun provideNavController(): NavController {
        return findNavController()
    }

    override fun addRecipes(recipes: List<RecipeViewModel>) {
        adapter.addItems(recipes)
    }

    override fun removeAllRecipes() {
        adapter.removeAllItems()
    }

    override fun setEmptyViewVisibility(isVisible: Boolean) {
        if (isVisible) {
            vwRecycler.visibility = View.GONE
            vwEmpty.visibility = View.VISIBLE
        } else {
            vwRecycler.visibility = View.VISIBLE
            vwEmpty.visibility = View.GONE
        }
    }

    override fun scrollToTop() {
        (vwRecycler.layoutManager as? LinearLayoutManager)?.smoothScrollToPosition(vwRecycler, null, 0)
    }

    /** endregion **/

    /** region ==================== Listeners ==================== **/

    private val onQueryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextChange(newText: String): Boolean {
            adapter.filter.filter(newText)
            return true
        }

        override fun onQueryTextSubmit(query: String): Boolean {
            return true
        }
    }

    private val recipeItemClickListener = object : RecipeItemClickListener {

        override fun onItemClick(viewModel: RecipeViewModel) {
            presenter.onRecipeItemClicked(viewModel)
        }

        override fun onEditClick(viewModel: RecipeViewModel) {
            presenter.onRecipeEditClicked(viewModel)
        }

        override fun onDeleteClick(viewModel: RecipeViewModel) {
            presenter.onRecipeDeleteClicked(viewModel)
        }
    }

    private val recipeListAdapterListener = object : RecipeListAdapterListener {

        override fun onFilterResult(isEmpty: Boolean) {
            setEmptyViewVisibility(isEmpty)
        }
    }

    private val onScrollListener = ListScrollListener()

    inner class ListScrollListener : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val layoutManager = this@HomeFragment.vwRecycler.layoutManager as LinearLayoutManager
            val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
            presenter.onListScrolled(lastVisibleItemPosition)
        }
    }

    /** endregion **/
}
