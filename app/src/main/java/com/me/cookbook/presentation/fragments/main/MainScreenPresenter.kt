package com.me.cookbook.presentation.fragments.main

import android.os.Bundle
import com.me.cookbook.R
import com.me.cookbook.data.auth.ServiceAuth
import com.me.cookbook.data.recipes.models.RecipeResponse
import com.me.cookbook.data.recipes.paginator.RecipesSearchPaginator
import com.me.cookbook.presentation.fragments.main.adapter.RecipeViewModel
import com.me.cookbook.presentation.other.resources.ResourceProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class MainScreenPresenter @Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val recipesSearchPaginator: RecipesSearchPaginator,
    private val serviceAuth: ServiceAuth
) : MainScreenContract.Presenter() {

    companion object {
        private const val LOAD_ADDITIONAL_EVENTS_THRESHOLD = 5
    }

    private var view: MainScreenContract.View? = null
    private val recipes = mutableListOf<RecipeResponse>()
    private var paginatorDisposable: Disposable? = null

    override fun attachView(view: MainScreenContract.View) {
        this.view = view
        observePaginator()
    }

    override fun onListScrolled(lastVisibleItemPosition: Int) {
        if (lastVisibleItemPosition > 0 &&
            lastVisibleItemPosition >= recipes.size + LOAD_ADDITIONAL_EVENTS_THRESHOLD &&
            recipesSearchPaginator.canLoadNext()
        ) {
            recipesSearchPaginator.next()
        }
    }

    override fun onQuerySubmit(query: String) {
        if (recipesSearchPaginator.setNewQuery(query)) {
            onPaginatorReset()
        }
    }

    override fun onReloadClicked() {
        recipesSearchPaginator.reset()
        onPaginatorReset()
    }

    override fun onRecipeItemClicked(viewModel: RecipeViewModel) {
        val id = viewModel.userInfo as Int? ?: return
        val recipe = recipes.find { it.id == id } ?: return

        val bundle = Bundle()
        bundle.putInt("recipeId", recipe.id)
        view?.provideNavController()?.navigate(R.id.to_recipeFragment, bundle)
    }

    override fun onRecipeImageClicked(viewModel: RecipeViewModel) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLoginClicked() {
        view?.provideNavController()?.navigate(R.id.nav_auth_graph)
    }

    override fun onLogoutClicked() {
        serviceAuth.logout()
        view?.provideNavController()?.navigate(R.id.to_splashActivity)
    }

    override fun onMenuReady() {
        view?.setLoginButtonVisibility(!serviceAuth.isAuthorized())
    }

    /** region ==================== Internal ==================== **/

    private fun observePaginator() {
        recipesSearchPaginator.reset()
        paginatorDisposable?.dispose()

        paginatorDisposable = recipesSearchPaginator.observable()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                view?.setEmptyViewVisibility(result.isEmpty())

                val newRecipes = result.filter { recipe -> recipes.none { it.id == recipe.id } }
                addNewRecipes(newRecipes)
            }, {
                view?.showError(resourceProvider.getString(R.string.paginator_load_error))
                onPaginatorReset()
            })

        compositeDisposable.add(paginatorDisposable!!)
    }

    private fun addNewRecipes(newRecipes: List<RecipeResponse>) {
        recipes.addAll(newRecipes)

        val viewModels = mutableListOf<RecipeViewModel>()
        newRecipes.forEach { (id, title, _, author, stepsCount, likesCount, _, imageUrl, _) ->

            viewModels.add(
                RecipeViewModel(
                    id,
                    title,
                    RecipeViewModel.Author(author.name),
                    likesCount.toString(),
                    stepsCount.toString(),
                    imageUrl
                )
            )
        }

        view?.addRecipes(viewModels)
    }

    private fun onPaginatorReset() {
        recipes.clear()
        view?.removeAllRecipes()
        observePaginator()
        recipesSearchPaginator.next()
    }

    /** endregion **/
}
