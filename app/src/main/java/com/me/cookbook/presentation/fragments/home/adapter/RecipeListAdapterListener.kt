package com.me.cookbook.presentation.fragments.home.adapter

interface RecipeListAdapterListener {

    fun onFilterResult(isEmpty: Boolean)
}
