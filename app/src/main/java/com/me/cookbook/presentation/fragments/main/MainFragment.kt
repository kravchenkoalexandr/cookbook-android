package com.me.cookbook.presentation.fragments.main

import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.me.cookbook.R
import com.me.cookbook.components.AppContext
import com.me.cookbook.presentation.fragments.main.adapter.RecipeItemClickListener
import com.me.cookbook.presentation.fragments.main.adapter.RecipeViewModel
import com.me.cookbook.presentation.fragments.main.adapter.RecipesListAdapter
import com.me.cookbook.utils.NotificationType
import com.me.cookbook.utils.makeToastNotifier
import javax.inject.Inject

class MainFragment : Fragment(), MainScreenContract.View {

    @Inject
    lateinit var presenter: MainScreenContract.Presenter

    @Inject
    lateinit var adapter: RecipesListAdapter

    private lateinit var vwRoot: View
    private lateinit var vwSearch: SearchView
    private lateinit var vwRecycler: RecyclerView
    private lateinit var vwEmpty: TextView

    private lateinit var miLogin: MenuItem
    private lateinit var miLogout: MenuItem

    /** Lifecycle **/

    override fun onCreate(savedInstanceState: Bundle?) {
        configureDI()
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureUI(view)

        presenter.attachView(this)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_recipes_toolbar_menu, menu)
        vwSearch = menu.findItem(R.id.action_search).actionView as SearchView
        vwSearch.setOnQueryTextListener(onQueryTextListener)
        vwSearch.queryHint = requireActivity().getString(R.string.search_view_hint)

        miLogin = menu.findItem(R.id.action_login)
        miLogout = menu.findItem(R.id.action_logout)

        presenter.onMenuReady()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_update -> {
                presenter.onReloadClicked()
                true
            }
            R.id.action_login -> {
                presenter.onLoginClicked()
                true
            }
            R.id.action_logout -> {
                presenter.onLogoutClicked()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /** Configure **/

    private fun configureUI(view: View) {
        vwRoot = view.findViewById(R.id.vw_root)
        vwRecycler = view.findViewById(R.id.rv_recipes)
        vwEmpty = view.findViewById(R.id.vw_empty)

        vwRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        vwRecycler.adapter = adapter
        vwRecycler.addOnScrollListener(onScrollListener)
    }

    private fun configureDI() {
        AppContext
            .appComponent
            .mainFragmentComponent(MainFragmentModule(onRecipeItemClickListener))
            .inject(this)
    }

    /** Contract **/

    override fun showError(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.ERROR
        ).show()
    }

    override fun showSuccess(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.SUCCESS
        ).show()
    }

    override fun provideNavController(): NavController {
        return findNavController()
    }

    override fun addRecipes(recipes: List<RecipeViewModel>) {
        adapter.addItems(recipes)
    }

    override fun removeAllRecipes() {
        adapter.removeAllItems()
    }

    override fun clearSearch() {
        vwSearch.setQuery("", true)
    }

    override fun setEmptyViewVisibility(isVisible: Boolean) {
        if (isVisible) {
            vwRecycler.visibility = View.GONE
            vwEmpty.visibility = View.VISIBLE
        } else {
            vwRecycler.visibility = View.VISIBLE
            vwEmpty.visibility = View.GONE
        }
    }

    override fun scrollToTop() {
        (vwRecycler.layoutManager as? LinearLayoutManager)?.smoothScrollToPosition(vwRecycler, null, 0)
    }

    override fun setLoginButtonVisibility(isVisible: Boolean) {
        miLogin.isVisible = isVisible
        miLogout.isVisible = !isVisible
    }

    /** region ==================== Listeners ==================== **/

    private val onQueryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextChange(newText: String): Boolean {
            return true
        }

        override fun onQueryTextSubmit(query: String): Boolean {
            presenter.onQuerySubmit(query)
            return true
        }
    }

    private val onRecipeItemClickListener = object : RecipeItemClickListener {

        override fun onItemClick(viewModel: RecipeViewModel) {
            presenter.onRecipeItemClicked(viewModel)
        }

        override fun onImageClick(viewModel: RecipeViewModel) {
            presenter.onRecipeImageClicked(viewModel)
        }
    }

    private val onScrollListener = ListScrollListener()

    inner class ListScrollListener : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val layoutManager = this@MainFragment.vwRecycler.layoutManager as LinearLayoutManager
            val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
            presenter.onListScrolled(lastVisibleItemPosition)
        }
    }

    /** endregion **/
}
