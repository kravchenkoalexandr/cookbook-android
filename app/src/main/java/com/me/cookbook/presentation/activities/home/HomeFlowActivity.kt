package com.me.cookbook.presentation.activities.home

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.me.cookbook.R
import com.me.cookbook.components.AppContext
import javax.inject.Inject
import kotlinx.android.synthetic.main.home_flow_activity.*
import kotlinx.android.synthetic.main.toolbar_default.*

class HomeFlowActivity : AppCompatActivity(), HomeFlowContract.View {

    @Inject
    lateinit var presenter: HomeFlowContract.Presenter

    /** region ==================== Lifecycle ==================== **/

    override fun onCreate(savedInstanceState: Bundle?) {
        configureDI()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_flow_activity)

        configureUI()

        presenter.attachView(this)
    }

    /** endregion **/

    /** region ==================== Config ==================== **/

    private fun configureUI() {
        setSupportActionBar(toolbar_default)
        supportActionBar?.title = ""

        val navController = findNavController(R.id.nav_host_fragment)

        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            configureDestinationBottomNavBar(destination)
            configureDestinationToolbar(destination)
        }

        vw_bottom_navigation.setupWithNavController(navController)
    }

    private fun configureDI() {
        AppContext.appComponent.homeFlowActivityComponent().inject(this)
    }

    private fun configureDestinationToolbar(destination: NavDestination) {
        when (destination.id) {
            R.id.mainFragment, R.id.homeFragment -> {
                toolbar_default.visibility = View.VISIBLE
            }
            else -> {
                toolbar_default.visibility = View.GONE
            }
        }
    }

    private fun configureDestinationBottomNavBar(destination: NavDestination) {
        when (destination.id) {
            R.id.recipeEditorFragment -> {
                vw_bottom_navigation.visibility = View.GONE
            }
            else -> {
                vw_bottom_navigation.visibility = View.VISIBLE
            }
        }
    }

    /** endregion **/
}
