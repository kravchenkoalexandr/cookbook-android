package com.me.cookbook.presentation.fragments.auth.login

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.me.cookbook.R
import com.me.cookbook.components.AppContext
import com.me.cookbook.extensions.focusEnd
import com.me.cookbook.utils.NotificationType
import com.me.cookbook.utils.makeToastNotifier
import javax.inject.Inject

class LoginFragment : Fragment(), LoginContract.View {

    companion object {

        private const val IMAGE_PICK_CODE = 1133
    }

    @Inject
    lateinit var presenter: LoginContract.Presenter

    private var isPasswordShow: Boolean = false

    private lateinit var vwRoot: View
    private lateinit var etEmail: EditText
    private lateinit var etPassword: EditText
    private lateinit var imgBtnShowHidePassword: ImageButton
    private lateinit var btnLogin: Button
    private lateinit var btnRegister: TextView
    private lateinit var btnContinueWithoutLogin: TextView

    /** Lifecycle **/

    override fun onCreate(savedInstanceState: Bundle?) {
        configureDI()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureUI(view)

        presenter.attachView(this)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    /** Configure **/

    private fun configureDI() {
        AppContext.appComponent.loginFragmentComponent().inject(this)
    }

    private fun configureUI(view: View) {
        vwRoot = view.findViewById(R.id.vw_root)
        etEmail = view.findViewById(R.id.et_email)
        etPassword = view.findViewById(R.id.et_password)
        imgBtnShowHidePassword = view.findViewById(R.id.btn_show_hide_password)
        btnLogin = view.findViewById(R.id.btn_login)
        btnRegister = view.findViewById(R.id.btn_register)
        btnContinueWithoutLogin = view.findViewById(R.id.btn_continue_without_login)

        btnLogin.setOnClickListener {
            presenter.onLoginClicked(etEmail.text.toString(), etPassword.text.toString())
        }

        btnContinueWithoutLogin.setOnClickListener {
            presenter.onContinueWithoutLoginClicked()
        }

        imgBtnShowHidePassword.setOnClickListener {
            switchShowHidePasswordButton()
        }

        btnRegister.setOnClickListener {
            presenter.onRegisterClicked()
        }
    }

    /** Contract **/

    override fun showSuccess(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.SUCCESS
        ).show()
    }

    override fun showError(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.ERROR
        ).show()
    }

    override fun clearFields() {
        etEmail.setText("")
        etPassword.setText("")
    }

    override fun focusEmail() {
        etEmail.focusEnd()
    }

    override fun focusPassword() {
        etPassword.focusEnd()
    }

    override fun provideNavController(): NavController {
        return findNavController()
    }

    /** Private **/

    private fun switchShowHidePasswordButton() {
        if (isPasswordShow) hidePassword() else showPassword()
        isPasswordShow = !isPasswordShow
        focusPassword()
    }

    private fun showPassword() {
        etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
        imgBtnShowHidePassword.setImageResource(R.drawable.ic_show_password)
    }

    private fun hidePassword() {
        etPassword.transformationMethod = PasswordTransformationMethod.getInstance()
        imgBtnShowHidePassword.setImageResource(R.drawable.ic_hide_password)
    }
}
