package com.me.cookbook.presentation.fragments.recipe.view

import com.me.cookbook.R
import com.me.cookbook.data.recipes.ServiceRecipes
import com.me.cookbook.data.recipes.models.RecipeResponse
import com.me.cookbook.presentation.fragments.recipe.view.adapter.RecipeHeaderViewModel
import com.me.cookbook.presentation.fragments.recipe.view.adapter.RecipeStepViewModel
import com.me.cookbook.presentation.fragments.recipe.view.adapter.RecipeViewModel
import com.me.cookbook.presentation.other.resources.ResourceProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class RecipePresenter @Inject constructor(
    private val serviceRecipes: ServiceRecipes,
    private val resourceProvider: ResourceProvider
) : RecipeContract.Presenter() {

    private var view: RecipeContract.View? = null

    private var loadRecipeDisposable: Disposable? = null

    override fun attachView(view: RecipeContract.View) {
        this.view = view
    }

    override fun loadRecipe(id: Int) {
        loadRecipeDisposable?.dispose()

        view?.clearRecipe()
        loadRecipeDisposable = serviceRecipes.getRecipeById(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                view?.setEmptyViewVisibility(false)
                view?.showRecipe(getRecipeViewModels(result))
            }, { error ->
                view?.setEmptyViewVisibility(true)
                view?.showError(resourceProvider.getString(R.string.recipe_load_error))
                error.printStackTrace()
            })

        compositeDisposable.addAll(loadRecipeDisposable!!)
    }

    private fun getRecipeViewModels(recipe: RecipeResponse): List<RecipeViewModel> {
        val viewModels = mutableListOf<RecipeViewModel>()
        viewModels.add(
            RecipeHeaderViewModel(
                recipe.id,
                recipe.title,
                recipe.description,
                RecipeHeaderViewModel.Author(
                    recipe.author.name
                ),
                recipe.likesCount.toString(),
                recipe.stepsCount.toString(),
                recipe.imageUrl
            )
        )

        recipe.steps.forEach { (id, title, body, _, number, imageUrl) ->
            viewModels.add(
                RecipeStepViewModel(
                    id,
                    title,
                    body,
                    number.toString(),
                    imageUrl
                )
            )
        }

        return viewModels
    }
}
