package com.me.cookbook.presentation.activities.splash

import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class SplashActivityModule {

    @Provides
    fun providePresenter(splashPresenter: SplashPresenter): SplashContract.Presenter {
        return splashPresenter
    }
}

@Subcomponent(modules = [SplashActivityModule::class])
interface SplashActivityComponent {

    fun inject(splashActivity: SplashActivity)
}
