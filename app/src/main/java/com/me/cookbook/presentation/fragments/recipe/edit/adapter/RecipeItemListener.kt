package com.me.cookbook.presentation.fragments.recipe.edit.adapter

interface RecipeItemListener {

    fun onImageClick(viewModel: RecipeViewModel)

    fun onItemRemoved(viewModel: RecipeViewModel)

    fun onItemMoved(viewModel: RecipeViewModel)
}
