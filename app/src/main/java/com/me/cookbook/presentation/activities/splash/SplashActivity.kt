package com.me.cookbook.presentation.activities.splash

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.me.cookbook.R
import com.me.cookbook.components.AppContext
import javax.inject.Inject
import kotlinx.android.synthetic.main.toolbar_default.*

class SplashActivity : AppCompatActivity(), SplashContract.View {

    @Inject
    lateinit var presenter: SplashContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        configureDI()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)

        configureUI()

        presenter.attachView(this)
    }

    private fun configureUI() {
        setSupportActionBar(toolbar_default)
        supportActionBar?.title = ""

        findNavController(R.id.nav_host_fragment)
            .addOnDestinationChangedListener { controller, destination, arguments ->
                when (destination.id) {
                    R.id.mainFragment -> {
                        toolbar_default.visibility = View.VISIBLE
                    }
                    else -> {
                        toolbar_default.visibility = View.GONE
                    }
                }
            }
    }

    private fun configureDI() {
        AppContext.appComponent.splashActivityComponent().inject(this)
    }
}
