package com.me.cookbook.presentation.fragments.home

import com.me.cookbook.presentation.fragments.home.adapter.RecipeItemClickListener
import com.me.cookbook.presentation.fragments.home.adapter.RecipeListAdapterListener
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class HomeFragmentModule(
    private val recipeItemClickListener: RecipeItemClickListener,
    private val recipeListAdapterListener: RecipeListAdapterListener
) {

    @Provides
    fun providePresenter(homePresenter: HomePresenter): HomeContract.Presenter {
        return homePresenter
    }

    @Provides
    fun provideRecipeItemClickListener(): RecipeItemClickListener {
        return recipeItemClickListener
    }

    @Provides
    fun provideRecipeListAdapterListener(): RecipeListAdapterListener {
        return recipeListAdapterListener
    }
}

@Subcomponent(modules = [HomeFragmentModule::class])
interface HomeFragmentComponent {

    fun inject(homeFragment: HomeFragment)
}
