package com.me.cookbook.presentation.activities.splash

import javax.inject.Inject

class SplashPresenter @Inject constructor() : SplashContract.Presenter() {

    private var view: SplashContract.View? = null

    override fun attachView(view: SplashContract.View) {
        this.view = view
    }
}
