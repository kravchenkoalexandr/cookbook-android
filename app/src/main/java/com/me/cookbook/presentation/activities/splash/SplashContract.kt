package com.me.cookbook.presentation.activities.splash

import com.me.cookbook.presentation.base.DisposablePresenter
import com.me.cookbook.presentation.base.MvpContract

interface SplashContract {

    abstract class Presenter : DisposablePresenter<View>()

    interface View : MvpContract.View
}
