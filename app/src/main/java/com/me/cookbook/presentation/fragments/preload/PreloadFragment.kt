package com.me.cookbook.presentation.fragments.preload

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.me.cookbook.R
import com.me.cookbook.components.AppContext
import javax.inject.Inject

class PreloadFragment : Fragment(), PreloadContract.View {

    @Inject
    lateinit var presenter: PreloadContract.Presenter

    private lateinit var vwRoot: View

    /** Lifecycle **/

    override fun onCreate(savedInstanceState: Bundle?) {
        configureDI()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureUI(view)

        presenter.attachView(this)
    }

    /** Configure **/

    private fun configureDI() {
        AppContext.appComponent.preloadFragmentComponent().inject(this)
    }

    private fun configureUI(view: View) {
        vwRoot = view.findViewById(R.id.vw_root)
    }

    /** Contract **/

    override fun provideNavController(): NavController {
        return findNavController()
    }
}
