package com.me.cookbook.presentation.base

interface MvpContract {

    interface View

    interface Presenter<V : View> {

        fun attachView(view: V)

        fun onDestroy()
    }
}
