package com.me.cookbook.presentation.other.resources

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import javax.inject.Inject

class ResourceProviderImpl @Inject constructor(
    private val context: Context
) : ResourceProvider {

    override fun getString(stringResId: Int): String {
        return context.resources.getString(stringResId)
    }

    override fun getString(stringResId: Int, vararg formatArgs: Any): String {
        return context.resources.getString(stringResId, *formatArgs)
    }

    override fun getQuantityString(pluralResId: Int, quantity: Int): String {
        return context.resources.getQuantityString(pluralResId, quantity)
    }

    override fun getDrawable(drawableResId: Int): Drawable? {
        return ContextCompat.getDrawable(context, drawableResId)
    }
}
