package com.me.cookbook.presentation.fragments.recipe.edit

import com.me.cookbook.presentation.fragments.recipe.edit.adapter.RecipeItemListener
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class RecipeEditorFragmentModule(private val recipeItemListener: RecipeItemListener) {

    @Provides
    fun providePresenter(recipeEditorPresenter: RecipeEditorPresenter): RecipeEditorContract.Presenter {
        return recipeEditorPresenter
    }

    @Provides
    fun provideRecipeItemClickListener(): RecipeItemListener {
        return recipeItemListener
    }
}

@Subcomponent(modules = [RecipeEditorFragmentModule::class])
interface RecipeEditorFragmentComponent {

    fun inject(recipeEditorFragment: RecipeEditorFragment)
}
