package com.me.cookbook.presentation.fragments.auth.registration

import androidx.navigation.NavController
import com.me.cookbook.presentation.base.DisposablePresenter
import com.me.cookbook.presentation.base.MvpContract

interface RegistrationContract {

    abstract class Presenter : DisposablePresenter<View>() {

        abstract fun onRegisterClicked(name: String, email: String, password: String)
    }

    interface View : MvpContract.View {

        fun showError(message: String)

        fun showSuccess(message: String)

        fun clearFields()

        fun focusEmail()

        fun focusPassword()

        fun focusName()

        fun provideNavController(): NavController
    }
}
