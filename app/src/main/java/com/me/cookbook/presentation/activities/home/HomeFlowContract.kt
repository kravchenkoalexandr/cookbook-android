package com.me.cookbook.presentation.activities.home

import com.me.cookbook.presentation.base.DisposablePresenter
import com.me.cookbook.presentation.base.MvpContract

interface HomeFlowContract {

    abstract class Presenter : DisposablePresenter<View>()

    interface View : MvpContract.View
}
