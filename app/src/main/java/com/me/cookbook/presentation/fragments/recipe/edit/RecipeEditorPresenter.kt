package com.me.cookbook.presentation.fragments.recipe.edit

import android.net.Uri
import android.util.Log
import com.me.cookbook.R
import com.me.cookbook.data.chefs.ServiceChefs
import com.me.cookbook.data.chefs.models.ChefResponse
import com.me.cookbook.data.cuisines.ServiceCuisines
import com.me.cookbook.data.cuisines.models.CuisinesResponse
import com.me.cookbook.data.files.ServiceFiles
import com.me.cookbook.data.recipes.ServiceRecipes
import com.me.cookbook.data.recipes.models.RecipeAddRequest
import com.me.cookbook.data.recipes.models.RecipeResponse
import com.me.cookbook.data.recipes.models.RecipeUpdateRequest
import com.me.cookbook.presentation.fragments.recipe.edit.adapter.RecipeHeaderViewModel
import com.me.cookbook.presentation.fragments.recipe.edit.adapter.RecipeStepViewModel
import com.me.cookbook.presentation.fragments.recipe.edit.adapter.RecipeViewModel
import com.me.cookbook.presentation.other.resources.ResourceProvider
import com.me.cookbook.utils.CacheFileProvider
import com.me.cookbook.utils.FileHelper
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.*
import javax.inject.Inject

class RecipeEditorPresenter @Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val serviceRecipes: ServiceRecipes,
    private val serviceFiles: ServiceFiles,
    private val serviceChefs: ServiceChefs,
    private val serviceCuisines: ServiceCuisines
) : RecipeEditorContract.Presenter() {

    enum class Mode {
        EDIT, ADD
    }

    private var view: RecipeEditorContract.View? = null
    private var recipeUploadDisposable: Disposable? = null
    private var chefsUploadDisposable: Disposable? = null
    private var cuisinesUploadDisposable: Disposable? = null
    private var chefs: List<ChefResponse> = emptyList()
    private var cuisines: List<CuisinesResponse> = emptyList()

    private var mode: Mode? = null
    private var viewModelToPickImage: RecipeViewModel? = null

    private var headerViewModel = createRecipeHeaderVM()
    private var stepsViewModels = mutableListOf<RecipeStepViewModel>()
    private var modelIds: Int = Int.MIN_VALUE + 1
    private val recipeViewModel: List<RecipeViewModel>
        get() = listOf(headerViewModel).plus(stepsViewModels)

    override fun attachView(view: RecipeEditorContract.View) {
        this.view = view

        uploadChefs()
        uploadCuisines()
    }

    override fun onViewDidLoadWithAddMode() {
        mode = Mode.ADD

        view?.updateRecipe(recipeViewModel)
    }

    override fun onViewDidLoadWithEditMode(recipe: RecipeResponse) {
        mode = Mode.EDIT

        initRecipeViewModel(recipe)
    }

    override fun onAcceptButtonClicked() {
        updateViewModels {
            if (!validateModels()) {
                showViewModels()
                view?.showError(resourceProvider.getString(R.string.recipe_editor_invalid_fields_values_error))
                return@updateViewModels
            } else {
                when (mode) {
                    Mode.EDIT -> updateRecipe()
                    Mode.ADD -> addRecipe()
                }
            }
        }
    }

    override fun onStepAddButtonClicked() {
        updateViewModels {
            stepsViewModels.add(createRecipeStepVM(stepsViewModels.size + 1))
            showViewModels()
        }
    }

    override fun onImageDeleted() {
        updateViewModels {
            updateViewModelImageUri(null)
            showViewModels()
        }
    }

    override fun onImagePicked(imageUri: Uri) {
        updateViewModels {
            updateViewModelImageUri(imageUri)
            showViewModels()
        }
    }

    override fun onRecipeImageClicked(viewModel: RecipeViewModel) {
        val targetViewModel = recipeViewModel.firstOrNull { it.modelId == viewModel.modelId }
        viewModelToPickImage = targetViewModel

        if (targetViewModel == null) { return }

        headerViewModelById(targetViewModel.modelId)?.let { header ->
            val imageUri = header.image.let { it.imageUrl ?: it.pickedImageUri }
            view?.pickImage(imageUri != null)
            return
        }

        stepViewModelById(targetViewModel.modelId)?.let { step ->
            val imageUri = step.image.let { it.imageUrl ?: it.pickedImageUri }
            view?.pickImage(imageUri != null)
            return
        }
    }

    override fun onItemMoved(viewModel: RecipeViewModel) {
        updateViewModels()
        showViewModels()
    }

    override fun onItemRemoved(viewModel: RecipeViewModel) {
        updateViewModels()
        showViewModels()
    }

    private fun showViewModels() {
        view?.updateRecipe(recipeViewModel)
    }

    private fun updateViewModelImageUri(pickerImageUri: Uri? = null) {
        viewModelToPickImage?.let { viewModelToPickImage ->
            headerViewModelById(viewModelToPickImage.modelId)?.let { header ->
                header.image.pickedImageUri = pickerImageUri
                this.viewModelToPickImage = null

                return
            }
        }

        viewModelToPickImage?.let { viewModelToPickImage ->
            stepViewModelById(viewModelToPickImage.modelId)?.let { step ->
                stepsViewModels
                    .first { it.modelId == step.modelId }
                    .image.pickedImageUri = pickerImageUri
                this.viewModelToPickImage = null

                return
            }
        }
    }

    private fun createRecipeHeaderVM(): RecipeHeaderViewModel {
        return RecipeHeaderViewModel(modelIds++, null, "", null, RecipeHeaderViewModel.Image(null, null), null, null)
    }

    private fun createRecipeStepVM(number: Int): RecipeStepViewModel {
        return RecipeStepViewModel(modelIds++, null, "", "", number, RecipeStepViewModel.Image(null, null))
    }

    private fun initRecipeViewModel(recipeResponse: RecipeResponse) {
        modelIds = Int.MIN_VALUE + 1

        headerViewModel = RecipeHeaderViewModel.ModelMapper.from(modelIds++, recipeResponse)
        stepsViewModels = recipeResponse.steps
            .map { RecipeStepViewModel.ModelMapper.from(modelIds++, it) }
            .toMutableList()
    }

    private fun updateViewModels(successBlock: (() -> Unit)? = null) {
        val newViewModels = view?.getRecipe() ?: return
        var newHeader: RecipeHeaderViewModel? = null
        val newSteps = mutableListOf<RecipeStepViewModel>()

        newViewModels.forEach { viewModel ->
            headerViewModelById(viewModel.modelId)?.let { newHeader = it }

            stepViewModelById(viewModel.modelId)?.let { newSteps.add(it) }
        }

        if (newHeader == null) {
            // TODO: Show error
            return
        }
        headerViewModel = newHeader!!
        stepsViewModels.clear()
        stepsViewModels.addAll(newSteps)
        stepsViewModels.forEachIndexed { index, step -> step.number = index + 1 }

        successBlock?.invoke()
    }

    private fun headerViewModelById(id: Int): RecipeHeaderViewModel? {
        return if (headerViewModel.modelId == id) headerViewModel else null
    }

    private fun stepViewModelById(id: Int): RecipeStepViewModel? {
        return stepsViewModels.firstOrNull { it.modelId == id }
    }

    private fun updateRecipe() {
        recipeUploadDisposable?.dispose()
        recipeUploadDisposable = Single.merge(buildFilesUploadRequests())
            .map { Log.d("RECIPE_EDITOR", "FILE URL: $it") }
            .ignoreElements()
            .andThen(serviceRecipes.update(buildRecipeUpdateRequest()))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view?.showSuccess(resourceProvider.getString(R.string.recipe_editor_recipe_update_success))
                    initRecipeViewModel(it)
                    showViewModels()
                },
                {
                    view?.showSuccess(resourceProvider.getString(R.string.recipe_editor_recipe_update_error))
                    it.printStackTrace()
                }
            )

        compositeDisposable.add(recipeUploadDisposable!!)
    }

    private fun addRecipe() {
        recipeUploadDisposable?.dispose()
        recipeUploadDisposable = Single.merge(buildFilesUploadRequests())
            .map { Log.d("RECIPE_EDITOR", "FILE URL: $it") }
            .ignoreElements()
            .andThen(serviceRecipes.add(buildRecipeAddRequest()))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view?.showSuccess(resourceProvider.getString(R.string.recipe_editor_recipe_add_success))
                    initRecipeViewModel(it)
                    mode = Mode.EDIT
                    showViewModels()
                },
                {
                    view?.showSuccess(resourceProvider.getString(R.string.recipe_editor_recipe_add_error))
                    it.printStackTrace()
                }
            )

        compositeDisposable.add(recipeUploadDisposable!!)
    }

    private fun buildFilesUploadRequests(): List<Single<String>> {
        val requests = mutableListOf<Single<String>>()

        headerViewModel.image.pickedImageUri?.let { uri ->
            val request = getImageUploadRequest(uri)
                .doAfterSuccess { headerViewModel.image.imageUrl = it }
            requests.add(request)
        }
        stepsViewModels.forEach { step ->
            step.image.pickedImageUri?.let { uri ->
                val request = getImageUploadRequest(uri)
                    .doAfterSuccess { step.image.imageUrl = it }
                requests.add(request)
            }
        }

        return requests
    }

    private fun getImageUploadRequest(pickedImageUri: Uri): Single<String> {
        val file = CacheFileProvider.createCacheImageFile()
        FileHelper.copyUriContentToFile(pickedImageUri, file)

        return serviceFiles.upload(file)
    }

    private fun buildRecipeAddRequest(): RecipeAddRequest {
        val steps = mutableListOf<RecipeAddRequest.Step>()

        stepsViewModels.forEach { stepViewModel ->
            with(stepViewModel) {
                steps.add(RecipeAddRequest.Step(title, body, number, image.imageUrl, null, null))
            }
        }

        return headerViewModel.let { headerViewModel ->
            val chef = headerViewModel.chef.let {
                if (it != null) {
                    RecipeAddRequest.Chef(it.userInfo as? Int, it.fullName)
                } else { null }
            }

            val cuisine = headerViewModel.nationalCuisine.let {
                if (it != null) {
                    RecipeAddRequest.NationalCuisine(it.userInfo as? Int, it.name)
                } else { null }
            }
            RecipeAddRequest(headerViewModel.title, headerViewModel.description, steps,
                headerViewModel.image.imageUrl, chef, cuisine)
        }
    }

    private fun buildRecipeUpdateRequest(): RecipeUpdateRequest {
        val steps = mutableListOf<RecipeUpdateRequest.Step>()

        stepsViewModels.forEach { stepViewModel ->
            with(stepViewModel) {
                steps.add(RecipeUpdateRequest.Step(userInfo as? Int, title, body, number, image.imageUrl))
            }
        }

        return headerViewModel.let { headerViewModel ->
            val chef = headerViewModel.chef.let {
                if (it != null) {
                    RecipeUpdateRequest.Chef(it.userInfo as? Int, it.fullName)
                } else { null }
            }

            val cuisine = headerViewModel.nationalCuisine.let {
                if (it != null) {
                    RecipeUpdateRequest.NationalCuisine(it.userInfo as? Int, it.name)
                } else { null }
            }

            RecipeUpdateRequest(headerViewModel.userInfo as Int, headerViewModel.title,
                headerViewModel.description, steps, headerViewModel.image.imageUrl,
                chef, cuisine)
        }
    }

    private fun uploadChefs() {
        chefsUploadDisposable?.dispose()
        chefsUploadDisposable = serviceChefs.getAll()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    chefs = it
                    view?.setChefs(chefs)
                },
                {
                    it.printStackTrace()
                }
            )

        compositeDisposable.add(chefsUploadDisposable!!)
    }

    private fun uploadCuisines() {
        cuisinesUploadDisposable?.dispose()
        cuisinesUploadDisposable = serviceCuisines.getAll()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    cuisines = it
                    view?.setCuisines(it)
                },
                {
                    it.printStackTrace()
                }
            )

        compositeDisposable.add(cuisinesUploadDisposable!!)
    }

    private fun validateModels(): Boolean {
        var isValid = validateHeaderViewModel()

        stepsViewModels.forEach { isValid = isValid && validateStepViewModel(it) }

        return isValid
    }

    private fun validateHeaderViewModel(): Boolean {
        var isValid = true

        with(headerViewModel) {
            titleError = ""
            chefError = ""
            recipeTypeError = ""

            if (title.isBlank()) {
                titleError = resourceProvider.getString(R.string.recipe_editor_recipe_title_empty_error)
                isValid = false
            }
            if (title.length >= 300) {
                titleError = resourceProvider.getString(R.string.recipe_editor_recipe_title_overflow_error)
                isValid = false
            }
            chef?.let {
                if (it.fullName.isBlank()) {
                    chefError = resourceProvider.getString(R.string.recipe_editor_recipe_chef_not_selected_error)
                    isValid = false
                }
            }
            if (chef == null && nationalCuisine == null) {
                recipeTypeError = resourceProvider.getString(R.string.recipe_editor_recipe_recipe_type_not_selected_error)
                isValid = false
            }
        }

        return isValid
    }

    private fun validateStepViewModel(stepViewModel: RecipeStepViewModel): Boolean {
        var isValid = true

        with(stepViewModel) {
            titleError = ""
            bodyError = ""

            if (title.isBlank()) {
                titleError = resourceProvider.getString(R.string.recipe_editor_step_title_empty_error)
                isValid = false
            }
            if (title.length >= 300) {
                titleError = resourceProvider.getString(R.string.recipe_editor_step_title_overflow_error)
                isValid = false
            }
            if (body.isBlank()) {
                bodyError = resourceProvider.getString(R.string.recipe_editor_step_body_empty_error)
                isValid = false
            }
            if (body.length >= 300) {
                bodyError = resourceProvider.getString(R.string.recipe_editor_step_body_overflow_error)
                isValid = false
            }
        }

        return isValid
    }
}
