package com.me.cookbook.presentation.fragments.recipe.view

import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class RecipeFragmentModule {

    @Provides
    fun providePresenter(recipePresenter: RecipePresenter): RecipeContract.Presenter {
        return recipePresenter
    }
}

@Subcomponent(modules = [RecipeFragmentModule::class])
interface RecipeFragmentComponent {

    fun inject(recipeFragment: RecipeFragment)
}
