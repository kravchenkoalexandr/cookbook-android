package com.me.cookbook.presentation.fragments.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.me.cookbook.R
import com.me.cookbook.extensions.getStepsCountImage
import com.me.cookbook.extensions.loadFromUriString
import com.me.cookbook.presentation.base.BaseViewHolder
import com.me.cookbook.presentation.other.resources.ResourceProvider
import javax.inject.Inject

class RecipesListAdapter @Inject constructor(
    private var recipeItemClickListener: RecipeItemClickListener,
    private var resourceProvider: ResourceProvider
) : RecyclerView.Adapter<BaseViewHolder>() {

    private var source = mutableListOf<RecipeViewModel>()

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recipe_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return source.size
    }

    fun addItems(items: List<RecipeViewModel>) {
        source.addAll(items)
        notifyDataSetChanged()
    }

    fun removeAllItems() {
        source.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(item: View) :
        BaseViewHolder(item),
        View.OnClickListener {

        private val imgRecipe: ImageView = itemView.findViewById(R.id.img_recipe)
        private val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
        private val imgTvAuthor: TextView = itemView.findViewById(R.id.tv_author)
        private val imgTvLikes: TextView = itemView.findViewById(R.id.tv_likes)
        private val imgTvSteps: TextView = itemView.findViewById(R.id.tv_steps)
        private lateinit var viewModel: RecipeViewModel

        init {
            itemView.setOnClickListener(this@ViewHolder)
        }

        override fun clearViewHolder() {
            imgRecipe.visibility = View.GONE
            tvTitle.text = ""
            imgTvAuthor.text = ""
            imgTvLikes.text = ""
            imgTvSteps.text = ""
        }

        override fun onBind(position: Int) {
            super.onBind(position)

            viewModel = source[position]

            viewModel.imageUrl?.let {
                imgRecipe.loadFromUriString(
                    it,
                    R.drawable.ic_dish,
                    null,
                    {
                        imgRecipe.visibility = View.VISIBLE
                        imgRecipe.setOnClickListener {
                            recipeItemClickListener.onImageClick(viewModel)
                        }
                    },
                    { imgRecipe.visibility = View.GONE }
                )
            }

            tvTitle.text = viewModel.title
            imgTvAuthor.text = resourceProvider.getString(R.string.recipe_author, viewModel.author.name)
            imgTvLikes.text = viewModel.likesCount

            val stepsCount = viewModel.stepsCount.toInt()
            imgTvSteps.text = resourceProvider.getQuantityString(R.plurals.steps_plurals, stepsCount)
            imgTvSteps.setCompoundDrawablesWithIntrinsicBounds(
                resourceProvider.getStepsCountImage(stepsCount), null, null, null
            )
        }

        override fun onClick(v: View?) {
            recipeItemClickListener.onItemClick(viewModel)
        }
    }
}
