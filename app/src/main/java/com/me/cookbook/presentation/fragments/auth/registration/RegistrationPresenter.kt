package com.me.cookbook.presentation.fragments.auth.registration

import com.me.cookbook.R
import com.me.cookbook.data.auth.ServiceAuth
import com.me.cookbook.presentation.other.resources.ResourceProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class RegistrationPresenter @Inject constructor(
    private val serviceAuth: ServiceAuth,
    private val resourceProvider: ResourceProvider
) : RegistrationContract.Presenter() {

    private var registrationDisposable: Disposable? = null
    private var view: RegistrationContract.View? = null

    override fun onRegisterClicked(name: String, email: String, password: String) {
        if (name.isBlank()) {
            view?.let {
                it.showError(resourceProvider.getString(R.string.name_edit_text_required))
                it.focusEmail()
            }
            return
        }

        if (email.isBlank()) {
            view?.let {
                it.showError(resourceProvider.getString(R.string.email_edit_text_required))
                it.focusEmail()
            }
            return
        }

        if (password.isBlank()) {
            view?.let {
                it.showError(resourceProvider.getString(R.string.password_edit_text_required))
                it.focusEmail()
            }
            return
        }

        registrationDisposable?.dispose()
        registrationDisposable = serviceAuth.register(name, email, password)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view?.provideNavController()?.navigate(R.id.homeFlowActivity)
                },
                { error ->
                    view?.let {
                        it.showError(resourceProvider.getString(R.string.registration_error))
                        it.clearFields()
                        it.focusName()
                    }
                    error.printStackTrace()
                }
            )

        compositeDisposable.add(registrationDisposable!!)
    }

    override fun attachView(view: RegistrationContract.View) {
        this.view = view
    }
}
