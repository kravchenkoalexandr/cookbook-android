package com.me.cookbook.presentation.fragments.home.adapter

data class RecipeViewModel(
    val userInfo: Any?,
    val title: String,
    val likesCount: String,
    val stepsCount: String,
    val imageUrl: String?
)
