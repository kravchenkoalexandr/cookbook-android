package com.me.cookbook.presentation.fragments.recipe.view.adapter

sealed class RecipeViewModel(val userInfo: Any?)

class RecipeHeaderViewModel(
    userInfo: Any?,
    val title: String,
    val description: String?,
    val author: Author,
    val likesCount: String,
    val stepsCount: String,
    val imageUrl: String?
) : RecipeViewModel(userInfo) {

    data class Author(val name: String)
}

class RecipeStepViewModel(
    userInfo: Any?,
    val title: String,
    val body: String,
    val number: String,
    val imageUrl: String?
) : RecipeViewModel(userInfo)
