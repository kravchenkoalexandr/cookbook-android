package com.me.cookbook.presentation.fragments.home.adapter

interface RecipeItemClickListener {

    fun onItemClick(viewModel: RecipeViewModel)

    fun onEditClick(viewModel: RecipeViewModel)

    fun onDeleteClick(viewModel: RecipeViewModel)
}
