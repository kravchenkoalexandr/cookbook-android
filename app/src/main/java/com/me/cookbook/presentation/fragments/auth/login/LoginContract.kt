package com.me.cookbook.presentation.fragments.auth.login

import androidx.navigation.NavController
import com.me.cookbook.presentation.base.DisposablePresenter
import com.me.cookbook.presentation.base.MvpContract

interface LoginContract {

    abstract class Presenter : DisposablePresenter<View>() {

        abstract fun onLoginClicked(email: String, password: String)

        abstract fun onRegisterClicked()

        abstract fun onContinueWithoutLoginClicked()
    }

    interface View : MvpContract.View {

        fun showError(message: String)

        fun showSuccess(message: String)

        fun clearFields()

        fun focusEmail()

        fun focusPassword()

        fun provideNavController(): NavController
    }
}
