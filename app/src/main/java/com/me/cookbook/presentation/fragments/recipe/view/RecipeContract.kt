package com.me.cookbook.presentation.fragments.recipe.view

import com.me.cookbook.presentation.base.DisposablePresenter
import com.me.cookbook.presentation.base.MvpContract
import com.me.cookbook.presentation.fragments.recipe.view.adapter.RecipeViewModel

interface RecipeContract {

    abstract class Presenter : DisposablePresenter<View>() {

        abstract fun loadRecipe(id: Int)
    }

    interface View : MvpContract.View {

        fun showError(message: String)

        fun showSuccess(message: String)

        fun showRecipe(viewModels: List<RecipeViewModel>)

        fun clearRecipe()

        fun setEmptyViewVisibility(isVisible: Boolean)
    }
}
