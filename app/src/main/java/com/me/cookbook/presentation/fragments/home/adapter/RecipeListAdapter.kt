package com.me.cookbook.presentation.fragments.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.me.cookbook.R
import com.me.cookbook.extensions.getStepsCountImage
import com.me.cookbook.extensions.loadFromUriString
import com.me.cookbook.presentation.base.BaseViewHolder
import com.me.cookbook.presentation.other.resources.ResourceProvider
import java.util.*
import javax.inject.Inject

class RecipesListAdapter @Inject constructor(
    private var recipeItemClickListener: RecipeItemClickListener,
    private var resourceProvider: ResourceProvider,
    private var adapterListener: RecipeListAdapterListener
) : RecyclerView.Adapter<BaseViewHolder>(), Filterable {

    private var query: String = ""
    private var items = mutableListOf<RecipeViewModel>()
    private var itemsToDisplay = mutableListOf<RecipeViewModel>()

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.user_recipe_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return itemsToDisplay.size
    }

    override fun getFilter(): Filter {
        return filter
    }

    fun addItems(items: List<RecipeViewModel>) {
        this.items.addAll(items)
        filter.filter(query)
    }

    fun removeAllItems() {
        items.clear()
        itemsToDisplay.clear()
        notifyDataSetChanged()
    }

    /** region ==================== Filter ==================== **/

    private val filter = object : Filter() {

        private val filterResults = FilterResults()

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            itemsToDisplay.clear()

            if (constraint.isNullOrBlank()) {
                itemsToDisplay.addAll(items)
            } else {
                query = constraint.toString().toLowerCase(Locale.getDefault()).trim()

                itemsToDisplay.addAll(items.filter {
                    it.title.toLowerCase(Locale.getDefault()).contains(query)
                })
            }

            return filterResults.also { it.values = itemsToDisplay }
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            adapterListener.onFilterResult(itemsToDisplay.isEmpty())
            notifyDataSetChanged()
        }
    }

    /** endregion **/

    /** region ==================== ViewHolder ==================== **/

    inner class ViewHolder(item: View) :
        BaseViewHolder(item),
        View.OnClickListener {

        private val imgRecipe: ImageView = itemView.findViewById(R.id.img_recipe)
        private val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
        private val imgTvLikes: TextView = itemView.findViewById(R.id.tv_likes)
        private val imgTvSteps: TextView = itemView.findViewById(R.id.tv_steps)
        private val btnEdit: ImageButton = itemView.findViewById(R.id.btn_edit)
        private val btnDelete: ImageButton = itemView.findViewById(R.id.btn_delete)

        private lateinit var viewModel: RecipeViewModel

        init {
            itemView.setOnClickListener(this@ViewHolder)
        }

        override fun clearViewHolder() {
            imgRecipe.visibility = View.GONE
            tvTitle.text = ""
            imgTvLikes.text = ""
            imgTvSteps.text = ""
        }

        override fun onBind(position: Int) {
            super.onBind(position)

            viewModel = itemsToDisplay[position]

            viewModel.imageUrl?.let {
                imgRecipe.loadFromUriString(
                    it,
                    R.drawable.ic_dish,
                    null,
                    { imgRecipe.visibility = View.VISIBLE },
                    { imgRecipe.visibility = View.GONE }
                )
            }

            tvTitle.text = viewModel.title
            imgTvLikes.text = viewModel.likesCount

            val stepsCount = viewModel.stepsCount.toInt()
            imgTvSteps.text = resourceProvider.getQuantityString(R.plurals.steps_plurals, stepsCount)
            imgTvSteps.setCompoundDrawablesWithIntrinsicBounds(
                resourceProvider.getStepsCountImage(stepsCount), null, null, null
            )

            btnEdit.setOnClickListener {
                recipeItemClickListener.onEditClick(viewModel)
            }

            btnDelete.setOnClickListener {
                recipeItemClickListener.onDeleteClick(viewModel)
            }
        }

        override fun onClick(v: View?) {
            recipeItemClickListener.onItemClick(viewModel)
        }
    }

    /** endregion **/
}
