package com.me.cookbook.presentation.fragments.recipe.edit

import android.net.Uri
import com.me.cookbook.data.chefs.models.ChefResponse
import com.me.cookbook.data.cuisines.models.CuisinesResponse
import com.me.cookbook.data.recipes.models.RecipeResponse
import com.me.cookbook.presentation.base.DisposablePresenter
import com.me.cookbook.presentation.base.MvpContract
import com.me.cookbook.presentation.fragments.recipe.edit.adapter.RecipeViewModel

interface RecipeEditorContract {

    abstract class Presenter : DisposablePresenter<View>() {

        abstract fun onViewDidLoadWithEditMode(recipe: RecipeResponse)

        abstract fun onViewDidLoadWithAddMode()

        abstract fun onAcceptButtonClicked()

        abstract fun onStepAddButtonClicked()

        abstract fun onRecipeImageClicked(viewModel: RecipeViewModel)

        abstract fun onImagePicked(imageUri: Uri)

        abstract fun onImageDeleted()

        abstract fun onItemMoved(viewModel: RecipeViewModel)

        abstract fun onItemRemoved(viewModel: RecipeViewModel)
    }

    interface View : MvpContract.View {

        fun showError(message: String)

        fun showSuccess(message: String)

        fun pickImage(isDeleteEnabled: Boolean)

        fun updateRecipe(viewModels: List<RecipeViewModel>)

        fun getRecipe(): List<RecipeViewModel>

        fun setEmptyViewVisibility(isVisible: Boolean)

        fun setChefs(chefs: List<ChefResponse>)

        fun setCuisines(cuisines: List<CuisinesResponse>)
    }
}
