package com.me.cookbook.presentation.fragments.auth.login

import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class LoginFragmentModule {

    @Provides
    fun providePresenter(loginPresenter: LoginPresenter): LoginContract.Presenter {
        return loginPresenter
    }
}

@Subcomponent(modules = [LoginFragmentModule::class])
interface LoginFragmentComponent {

    fun inject(loginFragment: LoginFragment)
}
