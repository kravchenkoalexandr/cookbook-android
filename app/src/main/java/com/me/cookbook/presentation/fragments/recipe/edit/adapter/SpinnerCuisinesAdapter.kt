package com.me.cookbook.presentation.fragments.recipe.edit.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.me.cookbook.R
import com.me.cookbook.data.cuisines.models.CuisinesResponse

class SpinnerCuisinesAdapter(context: Context, cuisines: List<CuisinesResponse>) :
    ArrayAdapter<CuisinesResponse>(context, 0, cuisines) {

    var items = cuisines.toList()
        private set

    fun setItems(items: List<CuisinesResponse>) {
        this.items = items.toList()
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(position: Int): CuisinesResponse? {
        return items.getOrNull(position)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    private fun initView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(context).inflate(
            R.layout.spinner_cuisines_list_item, parent, false)
        val tvAuthor: TextView = view.findViewById(R.id.tv_cuisines)

        val item = items[position]
        tvAuthor.text = item.name

        return view
    }
}
