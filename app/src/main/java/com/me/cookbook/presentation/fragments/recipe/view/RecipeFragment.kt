package com.me.cookbook.presentation.fragments.recipe.view

import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.me.cookbook.R
import com.me.cookbook.components.AppContext
import com.me.cookbook.presentation.fragments.recipe.view.adapter.RecipeListAdapter
import com.me.cookbook.presentation.fragments.recipe.view.adapter.RecipeViewModel
import com.me.cookbook.utils.NotificationType
import com.me.cookbook.utils.makeToastNotifier
import javax.inject.Inject

class RecipeFragment : Fragment(),
    RecipeContract.View {

    @Inject
    lateinit var presenter: RecipeContract.Presenter

    @Inject
    lateinit var adapter: RecipeListAdapter

    var recipeId: Int? = null

    lateinit var vwRoot: View
    lateinit var vwRecycler: RecyclerView
    lateinit var vwEmpty: TextView

    /** region ==================== Lifecycle ==================== **/

    override fun onCreate(savedInstanceState: Bundle?) {
        configureDI()
        recipeId = arguments?.getInt("recipeId")
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.recipe_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureUI(view)

        presenter.attachView(this)
        recipeId?.let { presenter.loadRecipe(it) }
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    /** endregion **/

    /** region ==================== Configure ==================== **/

    private fun configureDI() {
        AppContext.appComponent.recipeFragmentComponent().inject(this)
    }

    private fun configureUI(view: View) {
        vwRoot = view.findViewById(R.id.vw_root)
        vwRecycler = view.findViewById(R.id.rv_recipe)
        vwEmpty = view.findViewById(R.id.vw_empty)

        vwRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        vwRecycler.adapter = adapter
    }

    /** endregion **/

    /** region ==================== Contract ==================== **/

    override fun showSuccess(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.SUCCESS
        ).show()
    }

    override fun showError(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.ERROR
        ).show()
    }

    override fun showRecipe(viewModels: List<RecipeViewModel>) {
        adapter.addItems(viewModels)
    }

    override fun clearRecipe() {
        adapter.removeAllItems()
    }

    override fun setEmptyViewVisibility(isVisible: Boolean) {
        if (isVisible) {
            vwRecycler.visibility = View.GONE
            vwEmpty.visibility = View.VISIBLE
        } else {
            vwRecycler.visibility = View.VISIBLE
            vwEmpty.visibility = View.GONE
        }
    }

    /** endregion **/
}
