package com.me.cookbook.presentation.fragments.main.adapter

data class RecipeViewModel(
    val userInfo: Any?,
    val title: String,
    val author: Author,
    val likesCount: String,
    val stepsCount: String,
    val imageUrl: String?
) {

    data class Author(
        val name: String
    )
}
