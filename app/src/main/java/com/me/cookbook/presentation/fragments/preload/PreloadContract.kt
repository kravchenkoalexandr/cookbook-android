package com.me.cookbook.presentation.fragments.preload

import androidx.navigation.NavController
import com.me.cookbook.presentation.base.DisposablePresenter
import com.me.cookbook.presentation.base.MvpContract

interface PreloadContract {

    abstract class Presenter : DisposablePresenter<View>()

    interface View : MvpContract.View {

        fun provideNavController(): NavController
    }
}
