package com.me.cookbook.presentation.fragments.home

import androidx.navigation.NavController
import com.me.cookbook.presentation.base.DisposablePresenter
import com.me.cookbook.presentation.base.MvpContract
import com.me.cookbook.presentation.fragments.home.adapter.RecipeViewModel

interface HomeContract {

    abstract class Presenter : DisposablePresenter<View>() {

        abstract fun onListScrolled(lastVisibleItemPosition: Int)

        abstract fun onReloadClicked()

        abstract fun onRecipeItemClicked(viewModel: RecipeViewModel)

        abstract fun onRecipeEditClicked(viewModel: RecipeViewModel)

        abstract fun onRecipeDeleteClicked(viewModel: RecipeViewModel)

        abstract fun onRecipeAddClicked()

        abstract fun onLogoutClicked()
    }

    interface View : MvpContract.View {

        fun showError(message: String)

        fun showSuccess(message: String)

        fun setEmptyViewVisibility(isVisible: Boolean)

        fun addRecipes(recipes: List<RecipeViewModel>)

        fun removeAllRecipes()

        fun scrollToTop()

        fun provideNavController(): NavController
    }
}
