package com.me.cookbook.presentation.fragments.preload

import com.me.cookbook.R
import com.me.cookbook.presentation.other.resources.ResourceProvider
import com.me.cookbook.storages.credentials.CredentialsStorage
import com.me.cookbook.storages.settings.SettingsKeys
import com.me.cookbook.storages.settings.SettingsStorage
import javax.inject.Inject

class PreloadPresenter @Inject constructor(
    private val credentialsStorage: CredentialsStorage,
    private val resourceProvider: ResourceProvider,
    private val settingsStorage: SettingsStorage
) : PreloadContract.Presenter() {

    private var view: PreloadContract.View? = null

    override fun attachView(view: PreloadContract.View) {
        this.view = view

        val navController = view.provideNavController()

        when {
            credentialsStorage.getToken() != null ->
                navController.navigate(R.id.preloadFragment_to_homeFlowActivity)
            settingsStorage.getSetting(SettingsKeys.isContinueWithoutLogin) == true ->
                navController.navigate(R.id.to_mainFragment)
            else ->
                navController.navigate(R.id.preloadFragment_to_nav_auth_graph)
        }
    }
}
