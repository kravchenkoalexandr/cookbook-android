package com.me.cookbook.presentation.fragments.auth.registration

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.me.cookbook.R
import com.me.cookbook.components.AppContext
import com.me.cookbook.extensions.focusEnd
import com.me.cookbook.utils.NotificationType
import com.me.cookbook.utils.makeToastNotifier
import javax.inject.Inject

class RegistrationFragment : Fragment(), RegistrationContract.View {

    @Inject
    lateinit var presenter: RegistrationContract.Presenter

    private var isPasswordShow = false

    private lateinit var vwRoot: View
    private lateinit var etName: EditText
    private lateinit var etEmail: EditText
    private lateinit var etPassword: EditText
    private lateinit var imgBtnShowHidePassword: ImageButton
    private lateinit var btnRegister: Button

    /** Lifecycle **/

    override fun onCreate(savedInstanceState: Bundle?) {
        configureDI()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.registration_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureUI(view)

        presenter.attachView(this)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    /** Configure **/

    private fun configureDI() {
        AppContext.appComponent.registrationFragmentComponent().inject(this)
    }

    private fun configureUI(view: View) {
        vwRoot = view.findViewById(R.id.vw_root)
        etName = view.findViewById(R.id.et_name)
        etEmail = view.findViewById(R.id.et_email)
        etPassword = view.findViewById(R.id.et_password)
        imgBtnShowHidePassword = view.findViewById(R.id.btn_show_hide_password)
        btnRegister = view.findViewById(R.id.btn_register)

        imgBtnShowHidePassword.setOnClickListener {
            switchShowHidePasswordButton()
        }

        btnRegister.setOnClickListener {
            presenter.onRegisterClicked(
                etName.text.toString(),
                etEmail.text.toString(),
                etPassword.text.toString()
            )
        }
    }

    /** Contract **/

    override fun showError(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.ERROR
        ).show()
    }

    override fun showSuccess(message: String) {
        makeToastNotifier(
            vwRoot,
            layoutInflater,
            requireContext(),
            message,
            NotificationType.SUCCESS
        ).show()
    }

    override fun provideNavController(): NavController {
        return findNavController()
    }

    override fun clearFields() {
        etEmail.setText("")
        etPassword.setText("")
    }

    override fun focusEmail() {
        etEmail.focusEnd()
    }

    override fun focusPassword() {
        etPassword.focusEnd()
    }

    override fun focusName() {
        etName.focusEnd()
    }

    /** Private **/

    private fun switchShowHidePasswordButton() {
        if (isPasswordShow) hidePassword() else showPassword()
        isPasswordShow = !isPasswordShow
        focusPassword()
    }

    private fun showPassword() {
        etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
        imgBtnShowHidePassword.setImageResource(R.drawable.ic_show_password)
    }

    private fun hidePassword() {
        etPassword.transformationMethod = PasswordTransformationMethod.getInstance()
        imgBtnShowHidePassword.setImageResource(R.drawable.ic_hide_password)
    }
}
