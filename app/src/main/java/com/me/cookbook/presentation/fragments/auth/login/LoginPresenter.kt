package com.me.cookbook.presentation.fragments.auth.login

import com.me.cookbook.R
import com.me.cookbook.data.auth.ServiceAuth
import com.me.cookbook.presentation.other.resources.ResourceProvider
import com.me.cookbook.storages.settings.SettingsKeys
import com.me.cookbook.storages.settings.SettingsStorage
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class LoginPresenter @Inject constructor(
    private val serviceAuth: ServiceAuth,
    private val resourceProvider: ResourceProvider,
    private val settingsStorage: SettingsStorage
) : LoginContract.Presenter() {

    private var loginDisposable: Disposable? = null
    private var view: LoginContract.View? = null

    override fun onLoginClicked(email: String, password: String) {
        if (email.isBlank()) {
            view?.let {
                it.showError(resourceProvider.getString(R.string.email_edit_text_required))
                it.focusEmail()
            }
            return
        }

        if (password.isBlank()) {
            view?.let {
                it.showError(resourceProvider.getString(R.string.password_edit_text_required))
                it.focusPassword()
            }
            return
        }

        loginDisposable?.dispose()
        loginDisposable = serviceAuth.login(email, password)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    val nav = view?.provideNavController()
                    nav?.navigate(R.id.homeFlowActivity)
                },
                { error ->
                    view?.let {
                        it.showError(resourceProvider.getString(R.string.login_error))
                        it.clearFields()
                        it.focusEmail()
                    }
                    error.printStackTrace()
                }
            )

        compositeDisposable.add(loginDisposable!!)
    }

    override fun onRegisterClicked() {
        view?.provideNavController()?.navigate(R.id.loginFragment_to_registrationFragment)
    }

    override fun onContinueWithoutLoginClicked() {
        settingsStorage.setSetting(SettingsKeys.isContinueWithoutLogin, true)
        view?.provideNavController()?.navigate(R.id.preloadFragment)
    }

    override fun attachView(view: LoginContract.View) {
        this.view = view
    }
}
