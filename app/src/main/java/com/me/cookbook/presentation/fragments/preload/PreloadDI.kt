package com.me.cookbook.presentation.fragments.preload

import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class PreloadFragmentModule {

    @Provides
    fun providePresenter(preloadPresenter: PreloadPresenter): PreloadContract.Presenter {
        return preloadPresenter
    }
}

@Subcomponent(modules = [PreloadFragmentModule::class])
interface PreloadFragmentComponent {

    fun inject(preloadFragment: PreloadFragment)
}
