package com.me.cookbook.presentation.fragments.main

import androidx.navigation.NavController
import com.me.cookbook.presentation.base.DisposablePresenter
import com.me.cookbook.presentation.base.MvpContract
import com.me.cookbook.presentation.fragments.main.adapter.RecipeViewModel

interface MainScreenContract {

    abstract class Presenter : DisposablePresenter<View>() {

        abstract fun onQuerySubmit(query: String)

        abstract fun onListScrolled(lastVisibleItemPosition: Int)

        abstract fun onReloadClicked()

        abstract fun onRecipeItemClicked(viewModel: RecipeViewModel)

        abstract fun onRecipeImageClicked(viewModel: RecipeViewModel)

        abstract fun onLoginClicked()

        abstract fun onLogoutClicked()

        abstract fun onMenuReady()
    }

    interface View : MvpContract.View {

        fun showError(message: String)

        fun showSuccess(message: String)

        fun setLoginButtonVisibility(isVisible: Boolean)

        fun setEmptyViewVisibility(isVisible: Boolean)

        fun addRecipes(recipes: List<RecipeViewModel>)

        fun removeAllRecipes()

        fun clearSearch()

        fun scrollToTop()

        fun provideNavController(): NavController
    }
}
