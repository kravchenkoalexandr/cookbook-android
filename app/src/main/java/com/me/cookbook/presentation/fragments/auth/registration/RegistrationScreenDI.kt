package com.me.cookbook.presentation.fragments.auth.registration

import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class RegistrationProfileModule {

    @Provides
    fun providePresenter(registrationPresenter: RegistrationPresenter): RegistrationContract.Presenter {
        return registrationPresenter
    }
}

@Subcomponent(modules = [RegistrationProfileModule::class])
interface RegistrationFragmentComponent {

    fun inject(registrationFragment: RegistrationFragment)
}
