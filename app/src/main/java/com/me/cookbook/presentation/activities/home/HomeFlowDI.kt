package com.me.cookbook.presentation.activities.home

import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class HomeFlowActivityModule {

    @Provides
    fun providePresenter(homeFlowPresenter: HomeFlowPresenter): HomeFlowContract.Presenter {
        return homeFlowPresenter
    }
}

@Subcomponent(modules = [HomeFlowActivityModule::class])
interface HomeFlowActivityComponent {

    fun inject(homeFlowActivity: HomeFlowActivity)
}
