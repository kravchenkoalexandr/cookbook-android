package com.me.cookbook.presentation.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder constructor(item: View) : RecyclerView.ViewHolder(item) {

    var currentPosition: Int = 0
        private set

    open fun onBind(position: Int) {
        currentPosition = position
        clearViewHolder()
    }

    protected abstract fun clearViewHolder()
}
