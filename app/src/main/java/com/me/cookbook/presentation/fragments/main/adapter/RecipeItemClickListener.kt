package com.me.cookbook.presentation.fragments.main.adapter

interface RecipeItemClickListener {

    fun onItemClick(viewModel: RecipeViewModel)

    fun onImageClick(viewModel: RecipeViewModel)
}
