package com.me.cookbook.presentation.fragments.recipe.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.me.cookbook.R
import com.me.cookbook.extensions.getStepsCountImage
import com.me.cookbook.extensions.loadFromUriString
import com.me.cookbook.presentation.base.BaseViewHolder
import com.me.cookbook.presentation.other.resources.ResourceProvider
import java.lang.RuntimeException
import javax.inject.Inject

class RecipeListAdapter @Inject constructor(
    private var resourceProvider: ResourceProvider
) : RecyclerView.Adapter<BaseViewHolder>() {

    companion object {
        private val RECIPE_HEADER = 0
        private val RECIPE_STEP = 1
    }

    private var items = mutableListOf<RecipeViewModel>()

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            RECIPE_HEADER -> {
                val view = LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.recipe_header_list_item, parent, false)
                ViewHolderRecipeHeader(view)
            }
            RECIPE_STEP -> {
                val view = LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.recipe_step_list_item, parent, false)
                ViewHolderRecipeStep(view)
            }
            else -> throw RuntimeException("Invalid type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is RecipeHeaderViewModel -> RECIPE_HEADER
            is RecipeStepViewModel -> RECIPE_STEP
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addItems(items: List<RecipeViewModel>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun removeAllItems() {
        items.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolderRecipeHeader(item: View) : BaseViewHolder(item) {

        private val imgRecipe: ImageView = itemView.findViewById(R.id.img_recipe)
        private val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
        private val tvDescription: TextView = itemView.findViewById(R.id.tv_description)
        private val imgTvAuthor: TextView = itemView.findViewById(R.id.tv_author)
        private val imgTvLikes: TextView = itemView.findViewById(R.id.tv_likes)
        private val imgTvSteps: TextView = itemView.findViewById(R.id.tv_steps)
        private lateinit var viewModel: RecipeHeaderViewModel

        override fun clearViewHolder() {
            imgRecipe.visibility = View.GONE
            tvDescription.visibility = View.GONE
            tvTitle.text = ""
            tvDescription.text = ""
            imgTvLikes.text = ""
            imgTvSteps.text = ""
        }

        override fun onBind(position: Int) {
            super.onBind(position)

            viewModel = items[position] as RecipeHeaderViewModel

            viewModel.imageUrl?.let {
                imgRecipe.loadFromUriString(
                    it,
                    R.drawable.ic_dish,
                    null,
                    { imgRecipe.visibility = View.VISIBLE },
                    { imgRecipe.visibility = View.GONE }
                )
            }

            viewModel.description?.let {
                tvDescription.visibility = View.VISIBLE
                tvDescription.text = resourceProvider.getString(R.string.recipe_description, it)
            }

            tvTitle.text = viewModel.title
            imgTvAuthor.text = resourceProvider.getString(R.string.recipe_author, viewModel.author.name)
            imgTvLikes.text = viewModel.likesCount

            val stepsCount = viewModel.stepsCount.toInt()
            imgTvSteps.text = resourceProvider.getQuantityString(R.plurals.steps_plurals, stepsCount)
            imgTvSteps.setCompoundDrawablesWithIntrinsicBounds(
                resourceProvider.getStepsCountImage(stepsCount), null, null, null
            )
        }
    }

    inner class ViewHolderRecipeStep(item: View) : BaseViewHolder(item) {

        private val imgStep: ImageView = itemView.findViewById(R.id.img_step)
        private val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
        private val tvBody: TextView = itemView.findViewById(R.id.tv_body)
        private val tvNumber: TextView = itemView.findViewById(R.id.tv_number)
        private lateinit var viewModel: RecipeStepViewModel

        override fun clearViewHolder() {
            imgStep.visibility = View.GONE
            tvTitle.text = ""
            tvBody.text = ""
            tvNumber.text = ""
        }

        override fun onBind(position: Int) {
            super.onBind(position)

            viewModel = items[position] as RecipeStepViewModel

            viewModel.imageUrl?.let {
                imgStep.loadFromUriString(
                    it,
                    R.drawable.ic_dish,
                    null,
                    { imgStep.visibility = View.VISIBLE },
                    { imgStep.visibility = View.GONE }
                )
            }

            tvTitle.text = viewModel.title
            tvBody.text = resourceProvider.getString(R.string.step_body, viewModel.body)
            tvNumber.text = viewModel.number
        }
    }
}
