package com.me.cookbook.presentation.fragments.recipe.edit.adapter

import android.content.Context
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import com.me.cookbook.R
import com.me.cookbook.data.chefs.models.ChefResponse
import com.me.cookbook.data.cuisines.models.CuisinesResponse
import com.me.cookbook.extensions.loadFromUriString
import com.me.cookbook.extensions.setOnTextChangeListener
import com.me.cookbook.extensions.setVisibleEnable
import com.me.cookbook.presentation.base.BaseViewHolder
import com.me.cookbook.presentation.base.InstantAutoComplete
import com.me.cookbook.presentation.fragments.recipe.edit.adapter.RecipeListAdapter.Type.RECIPE_HEADER
import com.me.cookbook.presentation.fragments.recipe.edit.adapter.RecipeListAdapter.Type.RECIPE_STEP
import com.me.cookbook.presentation.other.resources.ResourceProvider
import java.util.Collections.swap
import javax.inject.Inject

class RecipeListAdapter @Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val recipeItemListener: RecipeItemListener,
    private val context: Context
) : RecyclerView.Adapter<BaseViewHolder>() {

    /** region ==================== Variables ==================== **/

    object Type {
        val RECIPE_HEADER = 0
        val RECIPE_STEP = 1
    }

    private val items = mutableListOf<RecipeViewModel>()

    private var chefs: List<ChefResponse> = emptyList()
    private var cuisines: List<CuisinesResponse> = emptyList()

    /** endregion **/

    /** region ==================== Override ==================== **/

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            RECIPE_HEADER -> {
                val view = LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.recipe_header_editor_list_item, parent, false)
                ViewHolderRecipeHeader(view)
            }
            RECIPE_STEP -> {
                val view = LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.recipe_step_editor_list_item, parent, false)
                ViewHolderRecipeStep(view)
            }
            else -> throw RuntimeException("Invalid type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is RecipeHeaderViewModel -> RECIPE_HEADER
            is RecipeStepViewModel -> RECIPE_STEP
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /** endregion **/

    /** region ==================== Inputs ==================== **/

    fun updateItems(items: List<RecipeViewModel>) {
        with(this.items) {
            clear()
            addAll(items)
        }

        notifyDataSetChanged()
    }

    fun updateChefs(chefs: List<ChefResponse>) {
        this.chefs = chefs
        notifyDataSetChanged()
    }

    fun updateCuisines(cuisines: List<CuisinesResponse>) {
        this.cuisines = cuisines
        notifyDataSetChanged()
    }

    fun moveItem(fromPosition: Int, toPosition: Int): Boolean {
        val viewModel = items[fromPosition]

        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                swap(items, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                swap(items, i, i - 1)
            }
        }

        notifyItemMoved(fromPosition, toPosition)
        recipeItemListener.onItemMoved(viewModel)

        return true
    }

    fun removeItem(position: Int) {
        val viewModel = items[position]

        items.removeAt(position)

        notifyItemRemoved(position)
        recipeItemListener.onItemRemoved(viewModel)
    }

    fun getViewModels(): List<RecipeViewModel> {
        return items.toList()
    }

    /** endregion **/

    /** region ==================== Inner ViewHolderRecipeHeader ==================== **/

    inner class ViewHolderRecipeHeader(item: View) : BaseViewHolder(item) {

        /** region ==================== Variables ==================== **/

        private val imgRecipe: ImageView = itemView.findViewById(R.id.img_recipe)
        private val etTitle: EditText = itemView.findViewById(R.id.et_title)
        private val etTitleLayout: TextInputLayout = item.findViewById(R.id.et_title_layout)
        private val etDescription: EditText = itemView.findViewById(R.id.et_description)
        private val etDescriptionLayout: TextInputLayout = item.findViewById(R.id.et_description_layout)
        private val etRecipeTypeLayout: TextInputLayout = item.findViewById(R.id.et_recipe_type_layout)
        private val radioGroup: RadioGroup = item.findViewById(R.id.radio_group)
        private val rdBtnAuthorsDish: RadioButton = item.findViewById(R.id.rd_btn_authors_dish)
        private val rdBtnNationalCuisine: RadioButton = item.findViewById(R.id.rd_btn_national_cuisine)
        private val tvAuthorName: InstantAutoComplete = item.findViewById(R.id.tv_author_name)
        private val spCuisines: Spinner = item.findViewById(R.id.sp_cuisines)

        private var titleOnTextChangeListener: TextWatcher? = null
        private var descriptionOnTextChangeListener: TextWatcher? = null
        private var authorOnTextChangeListener: TextWatcher? = null

        private lateinit var viewModel: RecipeHeaderViewModel

        /** endregion **/

        /** region ==================== Override ==================== **/

        override fun clearViewHolder() {
            clearTitle()
            clearDescription()
            clearImageRecipe()
            clearAuthor()
            clearCuisine()
            clearRadioGroup()
        }

        override fun onBind(position: Int) {
            super.onBind(position)

            viewModel = items[position] as RecipeHeaderViewModel

            initTitle()
            initDescription()
            initRecipeImage()
            initAuthor()
            initCuisine()
            initRadioGroup()
            initRecipeType()
        }

        /** endregion **/

        /** region ==================== Clear ==================== **/

        private fun clearTitle() {
            etTitle.removeTextChangedListener(titleOnTextChangeListener)
            etTitle.text.clear()
            etTitleLayout.error = ""

            titleOnTextChangeListener = null
        }

        private fun clearDescription() {
            etDescription.removeTextChangedListener(descriptionOnTextChangeListener)
            etDescription.text.clear()
            etDescriptionLayout.error = ""

            descriptionOnTextChangeListener = null
        }

        private fun clearImageRecipe() {
            imgRecipe.setOnClickListener(null)
            imgRecipe.setImageResource(R.drawable.ic_dish)
        }

        private fun clearAuthor() {
            tvAuthorName.removeTextChangedListener(authorOnTextChangeListener)
            tvAuthorName.setAdapter(null)
            tvAuthorName.text.clear()
            tvAuthorName.visibility = View.GONE

            authorOnTextChangeListener = null
        }

        private fun clearCuisine() {
            spCuisines.onItemSelectedListener = null
            spCuisines.adapter = null
            spCuisines.visibility = View.GONE
        }

        private fun clearRadioGroup() {
            etDescriptionLayout.error = ""
            radioGroup.setOnCheckedChangeListener(null)
            rdBtnAuthorsDish.setOnClickListener(null)
            rdBtnNationalCuisine.setOnClickListener(null)
            radioGroup.clearCheck()
        }

        /** endregion **/

        /** region ==================== Init UI ==================== **/

        private fun initTitle() {
            etTitle.setText(viewModel.title)
            etTitleLayout.error = viewModel.titleError
            titleOnTextChangeListener = etTitle.setOnTextChangeListener {
                viewModel.title = it

                etTitleLayout.error = ""
                viewModel.titleError = ""
            }
        }

        private fun initDescription() {
            etDescription.setText(viewModel.description)
            descriptionOnTextChangeListener = etDescription.setOnTextChangeListener {
                viewModel.description = it
            }
        }

        private fun initRecipeImage() {
            val imageUri = viewModel.image.let { it.imageUrl ?: it.pickedImageUri?.toString() }
            imageUri?.let {
                imgRecipe.loadFromUriString(it, R.drawable.ic_dish, R.drawable.ic_dish)
            }
            imgRecipe.setOnClickListener { recipeItemListener.onImageClick(viewModel) }
        }

        private fun initAuthor() {
            tvAuthorName.setAdapter(AutoCompleteChefAdapter(context, chefs))
            authorOnTextChangeListener = tvAuthorName.setOnTextChangeListener { fullName ->
                val chef = (tvAuthorName.adapter as AutoCompleteChefAdapter)
                    .items.firstOrNull { it.fullName == fullName }

                viewModel.chef = RecipeHeaderViewModel.Chef(chef?.id, chef?.fullName ?: fullName)
                etRecipeTypeLayout.error = ""
                viewModel.chefError = ""
            }
        }

        private fun initCuisine() {
            spCuisines.adapter = SpinnerCuisinesAdapter(context, cuisines)
            spCuisines.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val cuisine = parent?.getItemAtPosition(position) as CuisinesResponse
                    viewModel.nationalCuisine = RecipeHeaderViewModel.NationalCuisine(cuisine.id, cuisine.name)
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    viewModel.nationalCuisine = null
                }
            }
        }

        private fun initRadioGroup() {
            radioGroup.setOnCheckedChangeListener { _, checkedId ->
                when (checkedId) {
                    R.id.rd_btn_authors_dish -> {
                        tvAuthorName.setVisibleEnable(true)
                        spCuisines.setVisibleEnable(false)
                        viewModel.nationalCuisine = null

                        viewModel.chef = RecipeHeaderViewModel.Chef(
                            viewModel.chef?.userInfo as? Int, viewModel.chef?.fullName ?: "")
                    }
                    R.id.rd_btn_national_cuisine -> {
                        spCuisines.setVisibleEnable(true)
                        tvAuthorName.setVisibleEnable(false)
                        viewModel.chef = null
                    }
                }
            }
        }

        private fun initRecipeType() {
            when {
                viewModel.chef != null -> {
                    initAuthorRecipeType()
                    radioGroup.check(R.id.rd_btn_authors_dish)
                }
                viewModel.nationalCuisine != null -> {
                    initCuisineRecipeType()
                    radioGroup.check(R.id.rd_btn_national_cuisine)
                }
                else -> etRecipeTypeLayout.error = viewModel.recipeTypeError
            }
        }

        private fun initAuthorRecipeType() {
            etRecipeTypeLayout.error = viewModel.chefError

            tvAuthorName.setText(viewModel.chef?.fullName)
        }

        private fun initCuisineRecipeType() {
            etRecipeTypeLayout.error = ""
            viewModel.chefError = ""

            val itemPosition = (spCuisines.adapter as SpinnerCuisinesAdapter)
                .items.indexOfFirst { it.name == viewModel.nationalCuisine?.name }

            if (itemPosition != -1) {
                spCuisines.setSelection(itemPosition)
            }
        }

        /** endregion **/
    }

    /** endregion **/

    /** region ==================== Inner ViewHolderRecipeStep ==================== **/

    inner class ViewHolderRecipeStep(item: View) : BaseViewHolder(item) {

        /** region ==================== Variables ==================== **/

        private val imgStep: ImageView = itemView.findViewById(R.id.img_step)
        private val etTitle: EditText = itemView.findViewById(R.id.et_title)
        private val etTitleLayout: TextInputLayout = item.findViewById(R.id.et_title)
        private val etBody: EditText = itemView.findViewById(R.id.et_body)
        private val etBodyLayout: TextInputLayout = item.findViewById(R.id.et_body_layout)
        private val etNumber: EditText = itemView.findViewById(R.id.et_number)
        private val imgBtnDelete: ImageButton = itemView.findViewById(R.id.btn_delete)

        private var titleOnTextChangeListener: TextWatcher? = null
        private var bodyOnTextChangeListener: TextWatcher? = null

        private lateinit var viewModel: RecipeStepViewModel

        /** endregion **/

        /** region ==================== Override ==================== **/

        override fun clearViewHolder() {
            clearTitle()
            clearBody()
            clearStepImage()
            clearNumber()
        }

        override fun onBind(position: Int) {
            super.onBind(position)

            viewModel = items[position] as RecipeStepViewModel

            initTitle()
            initBody()
            initStepImage()
            initNumber()
        }

        /** endregion **/

        /** region ==================== Clear ==================== **/

        private fun clearTitle() {
            etTitle.removeTextChangedListener(titleOnTextChangeListener)
            etTitle.text.clear()
            etTitleLayout.error = ""
            titleOnTextChangeListener = null
        }

        private fun clearBody() {
            etBody.removeTextChangedListener(bodyOnTextChangeListener)
            etBody.text.clear()
            etBodyLayout.error = ""
            bodyOnTextChangeListener = null
        }

        private fun clearStepImage() {
            imgStep.setOnClickListener(null)
            imgStep.setImageResource(R.drawable.ic_dish)
        }

        private fun clearNumber() {
            etNumber.text.clear()
        }

        /** endregion **/

        /** region ==================== Init UI ==================== **/

        private fun initTitle() {
            etTitle.setText(viewModel.title)
            etTitleLayout.error = viewModel.titleError
            titleOnTextChangeListener = etTitle.setOnTextChangeListener {
                viewModel.title = it

                etTitleLayout.error = ""
                viewModel.titleError = ""
            }
        }

        private fun initBody() {
            etBody.setText(viewModel.body)
            etBodyLayout.error = viewModel.bodyError
            bodyOnTextChangeListener = etBody.setOnTextChangeListener {
                viewModel.body = it

                etBodyLayout.error = ""
                viewModel.bodyError = ""
            }
        }

        private fun initStepImage() {
            val imageUri = viewModel.image.let { it.imageUrl ?: it.pickedImageUri?.toString() }
            imageUri?.let {
                imgStep.loadFromUriString(it, R.drawable.ic_dish, R.drawable.ic_dish)
            }
            imgStep.setOnClickListener { recipeItemListener.onImageClick(viewModel) }
        }

        private fun initNumber() {
            etNumber.setText(viewModel.number.toString())
            etNumber.isEnabled = false
        }

        /** endregion **/
    }

    /** endregion **/
}
