package com.me.cookbook.presentation.fragments.main

import com.me.cookbook.presentation.fragments.main.adapter.RecipeItemClickListener
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Module
class MainFragmentModule(private val recipeItemClickListener: RecipeItemClickListener) {

    @Provides
    fun providePresenter(mainScreenPresenter: MainScreenPresenter): MainScreenContract.Presenter {
        return mainScreenPresenter
    }

    @Provides
    fun provideRecipeItemClickListener(): RecipeItemClickListener {
        return recipeItemClickListener
    }
}

@Subcomponent(modules = [MainFragmentModule::class])
interface MainFragmentComponent {

    fun inject(mainFragment: MainFragment)
}
