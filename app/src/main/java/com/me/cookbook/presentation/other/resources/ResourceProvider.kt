package com.me.cookbook.presentation.other.resources

import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes

interface ResourceProvider {

    fun getString(@StringRes stringResId: Int): String

    fun getString(@StringRes stringResId: Int, vararg formatArgs: Any): String

    fun getQuantityString(@PluralsRes pluralResId: Int, quantity: Int): String

    fun getDrawable(@DrawableRes drawableResId: Int): Drawable?
}
