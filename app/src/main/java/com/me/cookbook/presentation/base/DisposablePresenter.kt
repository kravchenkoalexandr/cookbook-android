package com.me.cookbook.presentation.base

import io.reactivex.disposables.CompositeDisposable

abstract class DisposablePresenter<V : MvpContract.View> : MvpContract.Presenter<V> {

    protected val compositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}
