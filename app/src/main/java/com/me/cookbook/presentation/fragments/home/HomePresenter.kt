package com.me.cookbook.presentation.fragments.home

import android.os.Bundle
import com.me.cookbook.R
import com.me.cookbook.data.auth.ServiceAuth
import com.me.cookbook.data.recipes.models.RecipeResponse
import com.me.cookbook.data.recipes.paginator.UserRecipesPaginator
import com.me.cookbook.presentation.fragments.home.adapter.RecipeViewModel
import com.me.cookbook.presentation.other.resources.ResourceProvider
import com.me.cookbook.storages.user.UserStorage
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class HomePresenter @Inject constructor(
    private val resourceProvider: ResourceProvider,
    private val serviceAuth: ServiceAuth,
    private val userRecipesPaginator: UserRecipesPaginator,
    private val userStorage: UserStorage
) : HomeContract.Presenter() {

    companion object {
        private const val LOAD_ADDITIONAL_EVENTS_THRESHOLD = 5
    }

    private var view: HomeContract.View? = null

    private val recipes = mutableListOf<RecipeResponse>()
    private var paginatorDisposable: Disposable? = null
    private var userStorageDisposable: Disposable? = null

    override fun attachView(view: HomeContract.View) {
        this.view = view
        observeUserStorage()
        observePaginator()
    }

    override fun onListScrolled(lastVisibleItemPosition: Int) {
        if (lastVisibleItemPosition > 0 &&
            lastVisibleItemPosition >= recipes.size + LOAD_ADDITIONAL_EVENTS_THRESHOLD &&
            userRecipesPaginator.canLoadNext()
        ) {
            userRecipesPaginator.next()
        }
    }

    override fun onReloadClicked() {
        userRecipesPaginator.reset()
        onPaginatorReset()
    }

    override fun onLogoutClicked() {
        serviceAuth.logout()
        view?.provideNavController()?.navigate(R.id.to_splashActivity)
    }

    override fun onRecipeItemClicked(viewModel: RecipeViewModel) {
        val recipe = getRecipeByViewModel(viewModel) ?: return
        val bundles = Bundle().apply { putInt("recipeId", recipe.id) }

        view?.provideNavController()?.navigate(R.id.to_recipeFragment, bundles)
    }

    override fun onRecipeEditClicked(viewModel: RecipeViewModel) {
        val recipe = getRecipeByViewModel(viewModel) ?: return
        val bundles = Bundle().apply { putSerializable("recipe", recipe) }

        view?.provideNavController()?.navigate(R.id.homeFragment_to_recipeEditorFragment, bundles)
    }

    override fun onRecipeDeleteClicked(viewModel: RecipeViewModel) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecipeAddClicked() {
        view?.provideNavController()?.navigate(R.id.homeFragment_to_recipeEditorFragment)
    }

    private fun observeUserStorage() {
        userStorageDisposable?.dispose()

        userStorageDisposable = userStorage.observable()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ (id, _) ->
                if (userRecipesPaginator.setNewQuery(id)) {
                    onPaginatorReset()
                }
            }, {
                it.printStackTrace()
                observeUserStorage()
            })

        compositeDisposable.add(userStorageDisposable!!)
    }

    private fun observePaginator() {
        userRecipesPaginator.reset()
        paginatorDisposable?.dispose()

        paginatorDisposable = userRecipesPaginator.observable()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                view?.setEmptyViewVisibility(result.isEmpty())

                val newRecipes = result.filter { recipe -> recipes.none { it.id == recipe.id } }
                addNewRecipes(newRecipes)
            }, {
                view?.showError(resourceProvider.getString(R.string.paginator_load_error))
                onPaginatorReset()
            })

        compositeDisposable.add(paginatorDisposable!!)
    }

    private fun addNewRecipes(newRecipes: List<RecipeResponse>) {
        recipes.addAll(newRecipes)

        val viewModels = mutableListOf<RecipeViewModel>()
        newRecipes.forEach { (id, title, _, _, stepsCount, likesCount, _, imageUrl, _) ->

            viewModels.add(
                RecipeViewModel(
                    id,
                    title,
                    likesCount.toString(),
                    stepsCount.toString(),
                    imageUrl
                )
            )
        }

        view?.addRecipes(viewModels)
    }

    private fun onPaginatorReset() {
        recipes.clear()
        view?.removeAllRecipes()
        observePaginator()
        userRecipesPaginator.next()
    }

    private fun getRecipeByViewModel(viewModel: RecipeViewModel): RecipeResponse? {
        val id = (viewModel.userInfo as Int?) ?: return null

        return recipes.firstOrNull { it.id == id }
    }
}
