package com.me.cookbook.presentation.fragments.recipe.edit.adapter

import android.net.Uri
import com.me.cookbook.data.recipes.models.RecipeResponse

sealed class RecipeViewModel(val modelId: Int, val userInfo: Any?)

class RecipeHeaderViewModel(
    modelId: Int,
    userInfo: Any?,
    var title: String,
    var description: String?,
    var image: Image,
    var chef: Chef?,
    var nationalCuisine: NationalCuisine?,
    var titleError: String = "",
    var chefError: String = "",
    var recipeTypeError: String = ""
) : RecipeViewModel(modelId, userInfo) {

    data class Image(var imageUrl: String?, var pickedImageUri: Uri?)

    data class Chef(var userInfo: Any?, var fullName: String) {

        companion object {
            fun from(source: RecipeResponse.Chef): Chef {
                val (id, fullName) = source

                return Chef(id, fullName)
            }
        }
    }

    data class NationalCuisine(var userInfo: Any?, var name: String) {

        companion object {
            fun from(source: RecipeResponse.NationalCuisine): NationalCuisine {
                val (id, name) = source

                return NationalCuisine(id, name)
            }
        }
    }

    object ModelMapper {
        fun from(modelId: Int, source: RecipeResponse): RecipeHeaderViewModel {
            val (id, title, description, _, _, _, _, imageUrl, _, chef, nationalCuisine) = source
            val chefMapped = if (chef != null) { Chef.from(chef) } else { null }
            val nationalCuisineMapped = if (nationalCuisine != null) {
                NationalCuisine.from(nationalCuisine)
            } else { null }

            return RecipeHeaderViewModel(modelId, id, title, description,
                Image(imageUrl, null), chefMapped, nationalCuisineMapped)
        }
    }
}

class RecipeStepViewModel(
    id: Int,
    userInfo: Any?,
    var title: String,
    var body: String,
    var number: Int,
    var image: Image,
    var titleError: String = "",
    var bodyError: String = ""
) : RecipeViewModel(id, userInfo) {

    data class Image(var imageUrl: String?, var pickedImageUri: Uri?)

    object ModelMapper {
        fun from(modelId: Int, source: RecipeResponse.Step): RecipeStepViewModel {
            val (id, title, body, _, number, imageUrl) = source

            return RecipeStepViewModel(modelId, id, title, body, number, Image(imageUrl, null))
        }
    }
}
