package com.me.cookbook.presentation.activities.home

import javax.inject.Inject

class HomeFlowPresenter @Inject constructor() : HomeFlowContract.Presenter() {

    private var view: HomeFlowContract.View? = null

    override fun attachView(view: HomeFlowContract.View) {
        this.view = view
    }
}
